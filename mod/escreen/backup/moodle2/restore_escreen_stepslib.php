<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    mod_escreen
 * @subpackage backup-moodle2
 * @copyright 2010 onwards Eloy Lafuente (stronk7) {@link http://stronk7.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Define all the restore steps that will be used by the restore_escreen_activity_task
 */

/**
 * Structure step to restore one escreen activity
 */
class restore_escreen_activity_structure_step extends restore_activity_structure_step {

    protected function define_structure() {

        $paths = array();
        $userinfo = $this->get_setting_value('userinfo');

        $paths[] = new restore_path_element('escreen', '/activity/escreen');
        $paths[] = new restore_path_element('escreen_item', '/activity/escreen/items/item');
        if ($userinfo) {
            $paths[] = new restore_path_element('escreen_completed', '/activity/escreen/completeds/completed');
            $paths[] = new restore_path_element('escreen_value', '/activity/escreen/completeds/completed/values/value');
            $paths[] = new restore_path_element('escreen_tracking', '/activity/escreen/trackings/tracking');
        }

        // Return the paths wrapped into standard activity structure
        return $this->prepare_activity_structure($paths);
    }

    protected function process_escreen($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->course = $this->get_courseid();

        $data->timeopen = $this->apply_date_offset($data->timeopen);
        $data->timeclose = $this->apply_date_offset($data->timeclose);
        $data->timemodified = $this->apply_date_offset($data->timemodified);

        // insert the escreen record
        $newitemid = $DB->insert_record('escreen', $data);
        // immediately after inserting "activity" record, call this
        $this->apply_activity_instance($newitemid);
    }

    protected function process_escreen_item($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->escreen = $this->get_new_parentid('escreen');

        //dependitem
        $data->dependitem = $this->get_mappingid('escreen_item', $data->dependitem);

        $newitemid = $DB->insert_record('escreen_item', $data);
        $this->set_mapping('escreen_item', $oldid, $newitemid, true); // Can have files
    }

    protected function process_escreen_completed($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->escreen = $this->get_new_parentid('escreen');
        $data->userid = $this->get_mappingid('user', $data->userid);
        $data->timemodified = $this->apply_date_offset($data->timemodified);

        $newitemid = $DB->insert_record('escreen_completed', $data);
        $this->set_mapping('escreen_completed', $oldid, $newitemid);
    }

    protected function process_escreen_value($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->completed = $this->get_new_parentid('escreen_completed');
        $data->item = $this->get_mappingid('escreen_item', $data->item);
        $data->course_id = $this->get_courseid();

        $newitemid = $DB->insert_record('escreen_value', $data);
        $this->set_mapping('escreen_value', $oldid, $newitemid);
    }

    protected function process_escreen_tracking($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->escreen = $this->get_new_parentid('escreen');
        $data->completed = $this->get_mappingid('escreen_completed', $data->completed);
        $data->userid = $this->get_mappingid('user', $data->userid);

        $newitemid = $DB->insert_record('escreen_tracking', $data);
    }


    protected function after_execute() {
        // Add escreen related files, no need to match by itemname (just internally handled context)
        $this->add_related_files('mod_escreen', 'intro', null);
        $this->add_related_files('mod_escreen', 'page_after_submit', null);
        $this->add_related_files('mod_escreen', 'item', 'escreen_item');
    }
}
