<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * deletes an item of the escreen
 *
 * @author Andreas Grabs
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package mod_escreen
 */

require_once("../../config.php");
require_once("lib.php");
require_once('delete_item_form.php');

$id = required_param('id', PARAM_INT);
$deleteitem = required_param('deleteitem', PARAM_INT);

$PAGE->set_url('/mod/escreen/delete_item.php', array('id'=>$id, 'deleteitem'=>$deleteitem));

if (! $cm = get_coursemodule_from_id('escreen', $id)) {
    print_error('invalidcoursemodule');
}

if (! $course = $DB->get_record("course", array("id"=>$cm->course))) {
    print_error('coursemisconf');
}

if (! $escreen = $DB->get_record("escreen", array("id"=>$cm->instance))) {
    print_error('invalidcoursemodule');
}

$context = context_module::instance($cm->id);

require_login($course, true, $cm);

require_capability('mod/escreen:edititems', $context);

$mform = new mod_escreen_delete_item_form();
$newformdata = array('id'=>$id,
                    'deleteitem'=>$deleteitem,
                    'confirmdelete'=>'1');
$mform->set_data($newformdata);
$formdata = $mform->get_data();

if ($mform->is_cancelled()) {
    redirect('edit.php?id='.$id);
}

if (isset($formdata->confirmdelete) AND $formdata->confirmdelete == 1) {
    escreen_delete_item($formdata->deleteitem);
    redirect('edit.php?id=' . $id);
}


/// Print the page header
$strescreens = get_string("modulenameplural", "escreen");
$strescreen  = get_string("modulename", "escreen");

$PAGE->navbar->add(get_string('delete_item', 'escreen'));
$PAGE->set_heading($course->fullname);
$PAGE->set_title($escreen->name);
echo $OUTPUT->header();

/// Print the main part of the page
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
echo $OUTPUT->heading(format_string($escreen->name));
echo $OUTPUT->box_start('generalbox errorboxcontent boxaligncenter boxwidthnormal');
echo html_writer::tag('p', get_string('confirmdeleteitem', 'escreen'), array('class' => 'bold'));
print_string('relateditemsdeleted', 'escreen');
$mform->display();
echo $OUTPUT->box_end();

echo $OUTPUT->footer();


