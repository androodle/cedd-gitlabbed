<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * print the form to add or edit a escreen-instance
 *
 * @author Andreas Grabs
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package mod_escreen
 */

//It must be included from a Moodle page
if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');
}

require_once($CFG->dirroot.'/course/moodleform_mod.php');

class mod_escreen_mod_form extends moodleform_mod {

    public function definition() {
        global $CFG, $DB;

        $editoroptions = escreen_get_editor_options();

        $mform    =& $this->_form;

        //-------------------------------------------------------------------------------
        $mform->addElement('header', 'general', get_string('general', 'form'));

        $mform->addElement('text', 'name', get_string('name', 'escreen'), array('size'=>'64'));
        $mform->setType('name', PARAM_TEXT);
        $mform->addRule('name', null, 'required', null, 'client');
        $mform->addRule('name', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');

        $this->add_intro_editor(true, get_string('description', 'escreen'));

        //-------------------------------------------------------------------------------
        $mform->addElement('header', 'timinghdr', get_string('availability'));

        $mform->addElement('date_time_selector', 'timeopen', get_string('escreenopen', 'escreen'),
            array('optional' => true));

        $mform->addElement('date_time_selector', 'timeclose', get_string('escreenclose', 'escreen'),
            array('optional' => true));

        //-------------------------------------------------------------------------------
        $mform->addElement('header', 'escreenhdr', get_string('questionandsubmission', 'escreen'));

        $options=array();
        $options[1]  = get_string('anonymous', 'escreen');
        $options[2]  = get_string('non_anonymous', 'escreen');
        $mform->addElement('select',
                           'anonymous',
                           get_string('anonymous_edit', 'escreen'),
                           $options);

        // check if there is existing responses to this escreen
        if (is_numeric($this->_instance) AND
                    $this->_instance AND
                    $escreen = $DB->get_record("escreen", array("id"=>$this->_instance))) {

            $completed_escreen_count = escreen_get_completeds_group_count($escreen);
        } else {
            $completed_escreen_count = false;
        }

        if ($completed_escreen_count) {
            $multiple_submit_value = $escreen->multiple_submit ? get_string('yes') : get_string('no');
            $mform->addElement('text',
                               'multiple_submit_static',
                               get_string('multiplesubmit', 'escreen'),
                               array('size'=>'4',
                                    'disabled'=>'disabled',
                                    'value'=>$multiple_submit_value));
            $mform->setType('multiple_submit_static', PARAM_RAW);

            $mform->addElement('hidden', 'multiple_submit', '');
            $mform->setType('multiple_submit', PARAM_INT);
            $mform->addHelpButton('multiple_submit_static', 'multiplesubmit', 'escreen');
        } else {
            $mform->addElement('selectyesno',
                               'multiple_submit',
                               get_string('multiplesubmit', 'escreen'));

            $mform->addHelpButton('multiple_submit', 'multiplesubmit', 'escreen');
        }

        $mform->addElement('selectyesno', 'email_notification', get_string('email_notification', 'escreen'));
        $mform->addHelpButton('email_notification', 'email_notification', 'escreen');

        $mform->addElement('selectyesno', 'autonumbering', get_string('autonumbering', 'escreen'));
        $mform->addHelpButton('autonumbering', 'autonumbering', 'escreen');

        //-------------------------------------------------------------------------------
        $mform->addElement('header', 'aftersubmithdr', get_string('after_submit', 'escreen'));

        $mform->addElement('selectyesno', 'publish_stats', get_string('show_analysepage_after_submit', 'escreen'));

        $mform->addElement('editor',
                           'page_after_submit_editor',
                           get_string("page_after_submit", "escreen"),
                           null,
                           $editoroptions);
        $mform->setType('page_after_submit_editor', PARAM_RAW);

        $mform->addElement('editor',
                           'message_highrisk_editor',
                           get_string("message_highrisk", "escreen"),
                           null,
                           $editoroptions);
        $mform->setType('message_highrisk_editor', PARAM_RAW);
		
        $mform->addElement('text',
                           'message_highrisk_question_ids',
                           get_string("message_question_ids", "escreen"));
		$mform->setType('message_highrisk_question_ids', PARAM_TEXT);

        $mform->addElement('editor',
                           'message_edeq_editor',
                           get_string("message_edeq", "escreen"),
                           null,
                           $editoroptions);
        $mform->setType('message_edeq_editor', PARAM_RAW);
		
        $mform->addElement('text',
                           'message_edeq_question_ids',
                           get_string("message_question_ids", "escreen"));
		$mform->setType('message_edeq_question_ids', PARAM_TEXT);

        $mform->addElement('editor',
                           'message_underage_editor',
                           get_string("message_underage", "escreen"),
                           null,
                           $editoroptions);
        $mform->setType('message_underage_editor', PARAM_RAW);

		$mform->addElement('text',
                           'message_underage_question_ids',
                           get_string("message_question_ids", "escreen"));
		$mform->setType('message_underage_question_ids', PARAM_TEXT);

        $mform->addElement('text',
                           'site_after_submit',
                           get_string('url_for_continue', 'escreen'),
                           array('size'=>'64', 'maxlength'=>'255'));

        $mform->setType('site_after_submit', PARAM_TEXT);
        $mform->addHelpButton('site_after_submit', 'url_for_continue', 'escreen');
		
        //-------------------------------------------------------------------------------
        $mform->addElement('header', 'popupshdr', get_string('popups', 'escreen'));

        $mform->addElement('textarea',
                           'popup_suicidal',
                           get_string("popup_suicidal", "escreen"),
                           'rows="7" cols="80"');
		
        $mform->addElement('text',
                           'popup_suicidal_question_ids',
                           get_string("popup_question_ids", "escreen"));
		$mform->setType('popup_suicidal_question_ids', PARAM_TEXT);

        $mform->addElement('textarea',
                           'popup_edeq',
                           get_string("popup_edeq", "escreen"),
                           'rows="7" cols="80"');
		
        $mform->addElement('text',
                           'popup_edeq_question_ids',
                           get_string("popup_question_ids", "escreen"));
		$mform->setType('popup_edeq_question_ids', PARAM_TEXT);

        $mform->setType('message_underage_editor', PARAM_RAW);
		
        //-------------------------------------------------------------------------------
        $this->standard_coursemodule_elements();
        //-------------------------------------------------------------------------------
        // buttons
        $this->add_action_buttons();
    }

    public function data_preprocessing(&$default_values) {

        $editoroptions = escreen_get_editor_options();

        if ($this->current->instance) {
            // editing an existing escreen - let us prepare the added editor elements (intro done automatically)
            $draftitemid = file_get_submitted_draft_itemid('page_after_submit');
            $default_values['page_after_submit_editor']['text'] =
                                    file_prepare_draft_area($draftitemid, $this->context->id,
                                    'mod_escreen', 'page_after_submit', false,
                                    $editoroptions,
                                    $default_values['page_after_submit']);

            $default_values['page_after_submit_editor']['format'] = $default_values['page_after_submitformat'];
            $default_values['page_after_submit_editor']['itemid'] = $draftitemid;
			
            $draftitemid = file_get_submitted_draft_itemid('message_highrisk');
            $default_values['message_highrisk_editor']['text'] =
                                    file_prepare_draft_area($draftitemid, $this->context->id,
                                    'mod_escreen', 'message_highrisk', false,
                                    $editoroptions,
                                    $default_values['message_highrisk']);

            $default_values['message_highrisk_editor']['itemid'] = $draftitemid;
			
            $draftitemid = file_get_submitted_draft_itemid('message_underage');
            $default_values['message_underage_editor']['text'] =
                                    file_prepare_draft_area($draftitemid, $this->context->id,
                                    'mod_escreen', 'message_underage', false,
                                    $editoroptions,
                                    $default_values['message_underage']);

            $default_values['message_underage_editor']['itemid'] = $draftitemid;
			
            $draftitemid = file_get_submitted_draft_itemid('message_edeq');
            $default_values['message_edeq_editor']['text'] =
                                    file_prepare_draft_area($draftitemid, $this->context->id,
                                    'mod_escreen', 'message_edeq', false,
                                    $editoroptions,
                                    $default_values['message_edeq']);

            $default_values['message_edeq_editor']['itemid'] = $draftitemid;
			
        } else {
            // adding a new escreen instance
            $draftitemid = file_get_submitted_draft_itemid('page_after_submit_editor');

            // no context yet, itemid not used
            file_prepare_draft_area($draftitemid, null, 'mod_escreen', 'page_after_submit', false);
            $default_values['page_after_submit_editor']['text'] = '';
            $default_values['page_after_submit_editor']['format'] = editors_get_preferred_format();
            $default_values['page_after_submit_editor']['itemid'] = $draftitemid;
			
            $draftitemid = file_get_submitted_draft_itemid('message_edeq_editor');

            // no context yet, itemid not used
            file_prepare_draft_area($draftitemid, null, 'mod_escreen', 'message_edeq', false);
            $default_values['message_edeq_editor']['text'] = '';
            $default_values['message_edeq_editor']['format'] = editors_get_preferred_format();
            $default_values['message_edeq_editor']['itemid'] = $draftitemid;
			
            $draftitemid = file_get_submitted_draft_itemid('message_underage_editor');

            // no context yet, itemid not used
            file_prepare_draft_area($draftitemid, null, 'mod_escreen', 'message_underage', false);
            $default_values['message_underage_editor']['text'] = '';
            $default_values['message_underage_editor']['format'] = editors_get_preferred_format();
            $default_values['message_underage_editor']['itemid'] = $draftitemid;
			
            $draftitemid = file_get_submitted_draft_itemid('message_highrisk_editor');

            // no context yet, itemid not used
            file_prepare_draft_area($draftitemid, null, 'mod_escreen', 'message_highrisk', false);
            $default_values['message_highrisk_editor']['text'] = '';
            $default_values['message_highrisk_editor']['format'] = editors_get_preferred_format();
            $default_values['message_highrisk_editor']['itemid'] = $draftitemid;
			
        }

    }

    public function get_data() {
        $data = parent::get_data();
        if ($data) {
            $data->page_after_submitformat = $data->page_after_submit_editor['format'];
            $data->page_after_submit = $data->page_after_submit_editor['text'];
            $data->message_edeq = $data->message_edeq_editor['text'];
            $data->message_highrisk = $data->message_highrisk_editor['text'];
            $data->message_underage = $data->message_underage_editor['text'];

            if (!empty($data->completionunlocked)) {
                // Turn off completion settings if the checkboxes aren't ticked
                $autocompletion = !empty($data->completion) &&
                    $data->completion == COMPLETION_TRACKING_AUTOMATIC;
                if (!$autocompletion || empty($data->completionsubmit)) {
                    $data->completionsubmit=0;
                }
            }
        }

        return $data;
    }

    public function validation($data, $files) {
        $errors = parent::validation($data, $files);
        return $errors;
    }

    public function add_completion_rules() {
        $mform =& $this->_form;

        $mform->addElement('checkbox',
                           'completionsubmit',
                           '',
                           get_string('completionsubmit', 'escreen'));
        return array('completionsubmit');
    }

    public function completion_rule_enabled($data) {
        return !empty($data['completionsubmit']);
    }
}
