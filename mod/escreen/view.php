<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * the first page to view the escreen
 *
 * @author Andreas Grabs
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package mod_escreen
 */
require_once("../../config.php");
require_once("lib.php");

$id = required_param('id', PARAM_INT);
$courseid = optional_param('courseid', false, PARAM_INT);

$current_tab = 'view';

if (! $cm = get_coursemodule_from_id('escreen', $id)) {
    print_error('invalidcoursemodule');
}

if (! $course = $DB->get_record("course", array("id"=>$cm->course))) {
    print_error('coursemisconf');
}

if (! $escreen = $DB->get_record("escreen", array("id"=>$cm->instance))) {
    print_error('invalidcoursemodule');
}

$context = context_module::instance($cm->id);

$escreen_complete_cap = false;

if (has_capability('mod/escreen:complete', $context)) {
    $escreen_complete_cap = true;
}

if (isset($CFG->escreen_allowfullanonymous)
            AND $CFG->escreen_allowfullanonymous
            AND $course->id == SITEID
            AND (!$courseid OR $courseid == SITEID)
            AND $escreen->anonymous == ESCREEN_ANONYMOUS_YES ) {
    $escreen_complete_cap = true;
}

//check whether the escreen is located and! started from the mainsite
if ($course->id == SITEID AND !$courseid) {
    $courseid = SITEID;
}

//check whether the escreen is mapped to the given courseid
if ($course->id == SITEID AND !has_capability('mod/escreen:edititems', $context)) {
    if ($DB->get_records('escreen_sitecourse_map', array('escreenid'=>$escreen->id))) {
        $params = array('escreenid'=>$escreen->id, 'courseid'=>$courseid);
        if (!$DB->get_record('escreen_sitecourse_map', $params)) {
            print_error('invalidcoursemodule');
        }
    }
}

if ($escreen->anonymous != ESCREEN_ANONYMOUS_YES) {
    if ($course->id == SITEID) {
        require_login($course, true);
    } else {
        require_login($course, true, $cm);
    }
} else {
    if ($course->id == SITEID) {
        require_course_login($course, true);
    } else {
        require_course_login($course, true, $cm);
    }
}

//check whether the given courseid exists
if ($courseid AND $courseid != SITEID) {
    if ($course2 = $DB->get_record('course', array('id'=>$courseid))) {
        require_course_login($course2); //this overwrites the object $course :-(
        $course = $DB->get_record("course", array("id"=>$cm->course)); // the workaround
    } else {
        print_error('invalidcourseid');
    }
}

// Trigger module viewed event.
$event = \mod_escreen\event\course_module_viewed::create(array(
    'objectid' => $escreen->id,
    'context' => $context,
    'anonymous' => ($escreen->anonymous == ESCREEN_ANONYMOUS_YES),
    'other' => array(
        'anonymous' => $escreen->anonymous // Deprecated.
    )
));
$event->add_record_snapshot('course_modules', $cm);
$event->add_record_snapshot('course', $course);
$event->add_record_snapshot('escreen', $escreen);
$event->trigger();

/// Print the page header
$strescreens = get_string("modulenameplural", "escreen");
$strescreen  = get_string("modulename", "escreen");

if ($course->id == SITEID) {
    $PAGE->set_context($context);
    $PAGE->set_cm($cm, $course); // set's up global $COURSE
    $PAGE->set_pagelayout('incourse');
}
$PAGE->set_url('/mod/escreen/view.php', array('id'=>$cm->id, 'do_show'=>'view'));
$PAGE->set_title($escreen->name);
$PAGE->set_heading($course->fullname);
echo $OUTPUT->header();

//ishidden check.
//escreen in courses
$cap_viewhiddenactivities = has_capability('moodle/course:viewhiddenactivities', $context);
if ((empty($cm->visible) and !$cap_viewhiddenactivities) AND $course->id != SITEID) {
    notice(get_string("activityiscurrentlyhidden"));
}

//ishidden check.
//escreen on mainsite
if ((empty($cm->visible) and !$cap_viewhiddenactivities) AND $courseid == SITEID) {
    notice(get_string("activityiscurrentlyhidden"));
}

/// Print the main part of the page
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

$previewimg = $OUTPUT->pix_icon('t/preview', get_string('preview'));
$previewlnk = new moodle_url('/mod/escreen/print.php', array('id' => $id));
$preview = html_writer::link($previewlnk, $previewimg);

echo $OUTPUT->heading(format_string($escreen->name) . $preview);

// Print the tabs.
require('tabs.php');

//show some infos to the escreen
if (has_capability('mod/escreen:edititems', $context)) {
    //get the groupid
    $groupselect = groups_print_activity_menu($cm, $CFG->wwwroot.'/mod/escreen/view.php?id='.$cm->id, true);
    $mygroupid = groups_get_activity_group($cm);

    echo $OUTPUT->box_start('boxaligncenter boxwidthwide');
    echo $groupselect.'<div class="clearer">&nbsp;</div>';
    $completedscount = escreen_get_completeds_group_count($escreen, $mygroupid);
    echo $OUTPUT->box_start('escreen_info');
    echo '<span class="escreen_info">';
    echo get_string('completed_escreens', 'escreen').': ';
    echo '</span>';
    echo '<span class="escreen_info_value">';
    echo $completedscount;
    echo '</span>';
    echo $OUTPUT->box_end();

    $params = array('escreen'=>$escreen->id, 'hasvalue'=>1);
    $itemscount = $DB->count_records('escreen_item', $params);
    echo $OUTPUT->box_start('escreen_info');
    echo '<span class="escreen_info">';
    echo get_string('questions', 'escreen').': ';
    echo '</span>';
    echo '<span class="escreen_info_value">';
    echo $itemscount;
    echo '</span>';
    echo $OUTPUT->box_end();

    if ($escreen->timeopen) {
        echo $OUTPUT->box_start('escreen_info');
        echo '<span class="escreen_info">';
        echo get_string('escreenopen', 'escreen').': ';
        echo '</span>';
        echo '<span class="escreen_info_value">';
        echo userdate($escreen->timeopen);
        echo '</span>';
        echo $OUTPUT->box_end();
    }
    if ($escreen->timeclose) {
        echo $OUTPUT->box_start('escreen_info');
        echo '<span class="escreen_info">';
        echo get_string('escreenclose', 'escreen').': ';
        echo '</span>';
        echo '<span class="escreen_info_value">';
        echo userdate($escreen->timeclose);
        echo '</span>';
        echo $OUTPUT->box_end();
    }
    echo $OUTPUT->box_end();
}

if (has_capability('mod/escreen:edititems', $context)) {
    echo $OUTPUT->heading(get_string('description', 'escreen'), 3);
}
echo $OUTPUT->box_start('generalbox boxwidthwide');
$options = (object)array('noclean'=>true);
echo format_module_intro('escreen', $escreen, $cm->id);
echo $OUTPUT->box_end();

if (has_capability('mod/escreen:edititems', $context)) {
    require_once($CFG->libdir . '/filelib.php');

    $page_after_submit_output = file_rewrite_pluginfile_urls($escreen->page_after_submit,
                                                            'pluginfile.php',
                                                            $context->id,
                                                            'mod_escreen',
                                                            'page_after_submit',
                                                            0);

    echo $OUTPUT->heading(get_string("page_after_submit", "escreen"), 3);
    echo $OUTPUT->box_start('generalbox boxaligncenter boxwidthwide');
    echo format_text($page_after_submit_output,
                     $escreen->page_after_submitformat,
                     array('overflowdiv'=>true));

    echo $OUTPUT->box_end();
}

if ( (intval($escreen->publish_stats) == 1) AND
                ( has_capability('mod/escreen:viewanalysepage', $context)) AND
                !( has_capability('mod/escreen:viewreports', $context)) ) {

    $params = array('userid'=>$USER->id, 'escreen'=>$escreen->id);
    if ($multiple_count = $DB->count_records('escreen_tracking', $params)) {
        $url_params = array('id'=>$id, 'courseid'=>$courseid);
        $analysisurl = new moodle_url('/mod/escreen/analysis.php', $url_params);
        echo '<div class="mdl-align"><a href="'.$analysisurl->out().'">';
        echo get_string('completed_escreens', 'escreen').'</a>';
        echo '</div>';
    }
}

//####### mapcourse-start
if (has_capability('mod/escreen:mapcourse', $context)) {
    if ($escreen->course == SITEID) {
        echo $OUTPUT->box_start('generalbox boxaligncenter boxwidthwide');
        echo '<div class="mdl-align">';
        echo '<form action="mapcourse.php" method="get">';
        echo '<fieldset>';
        echo '<input type="hidden" name="sesskey" value="'.sesskey().'" />';
        echo '<input type="hidden" name="id" value="'.$id.'" />';
        echo '<button type="submit">'.get_string('mapcourses', 'escreen').'</button>';
        echo $OUTPUT->help_icon('mapcourse', 'escreen');
        echo '</fieldset>';
        echo '</form>';
        echo '<br />';
        echo '</div>';
        echo $OUTPUT->box_end();
    }
}
//####### mapcourse-end

//####### completed-start
if ($escreen_complete_cap) {
    echo $OUTPUT->box_start('generalbox boxaligncenter boxwidthwide');
    //check, whether the escreen is open (timeopen, timeclose)
    $checktime = time();
    if (($escreen->timeopen > $checktime) OR
            ($escreen->timeclose < $checktime AND $escreen->timeclose > 0)) {

        echo $OUTPUT->notification(get_string('escreen_is_not_open', 'escreen'));
        echo $OUTPUT->continue_button($CFG->wwwroot.'/course/view.php?id='.$course->id);
        echo $OUTPUT->box_end();
        echo $OUTPUT->footer();
        exit;
    }

    //check multiple Submit
    $escreen_can_submit = true;
    if ($escreen->multiple_submit == 0 ) {
        if (escreen_is_already_submitted($escreen->id, $courseid)) {
            $escreen_can_submit = false;
        }
    }
    if ($escreen_can_submit) {
        //if the user is not known so we cannot save the values temporarly
        if (!isloggedin() or isguestuser()) {
            $completefile = 'complete_guest.php';
            $guestid = sesskey();
        } else {
            $completefile = 'complete.php';
            $guestid = false;
        }
        $url_params = array('id'=>$id, 'courseid'=>$courseid, 'gopage'=>0);
        $completeurl = new moodle_url('/mod/escreen/'.$completefile, $url_params);

        $escreencompletedtmp = escreen_get_current_completed($escreen->id, true, $courseid, $guestid);
        if ($escreencompletedtmp) {
            if ($startpage = escreen_get_page_to_continue($escreen->id, $courseid, $guestid)) {
                $completeurl->param('gopage', $startpage);
            }
            echo '<a href="'.$completeurl->out().'">'.get_string('continue_the_form', 'escreen').'</a>';
        } else {
            echo '<a href="'.$completeurl->out().'">'.get_string('complete_the_form', 'escreen').'</a>';
        }
    } else {
        echo $OUTPUT->notification(get_string('this_escreen_is_already_submitted', 'escreen'));
        if ($courseid) {
            echo $OUTPUT->continue_button($CFG->wwwroot.'/course/view.php?id='.$courseid);
        } else {
            echo $OUTPUT->continue_button($CFG->wwwroot.'/course/view.php?id='.$course->id);
        }
    }
    echo $OUTPUT->box_end();
}
//####### completed-end

/// Finish the page
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

echo $OUTPUT->footer();

