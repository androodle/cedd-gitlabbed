<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once($CFG->dirroot.'/mod/escreen/item/escreen_item_form_class.php');

class escreen_textarea_form extends escreen_item_form {
    protected $type = "textarea";

    public function definition() {
        $item = $this->_customdata['item'];
        $common = $this->_customdata['common'];
        $positionlist = $this->_customdata['positionlist'];
        $position = $this->_customdata['position'];

        $mform =& $this->_form;

        $mform->addElement('header', 'general', get_string($this->type, 'escreen'));
        $mform->addElement('advcheckbox', 'required', get_string('required', 'escreen'), '' , null , array(0, 1));

        $mform->addElement('editor',
                            'name_editor',
                            get_string('item_name', 'escreen'));
		$mform->setType('name_editor', PARAM_RAW);
		
        $mform->addElement('text',
                            'label',
                            get_string('item_label', 'escreen'),
                            array('size'=>ESCREEN_ITEM_LABEL_TEXTBOX_SIZE, 'maxlength'=>255));

        $mform->addElement('select',
                            'itemwidth',
                            get_string('textarea_width', 'escreen').'&nbsp;',
                            array_slice(range(0, 80), 5, 80, true));

        $mform->addElement('select',
                            'itemheight',
                            get_string('textarea_height', 'escreen').'&nbsp;',
                            array_slice(range(0, 40), 5, 40, true));

        parent::definition();
        $this->set_data($item);

    }

    public function get_data() {
        if (!$item = parent::get_data()) {
            return false;
        }

        $item->presentation = $item->itemwidth . '|'. $item->itemheight;
		$item->name = $item->name_editor['text'];
        return $item;
    }
	
	public function set_data($item) {
		if (!empty($item->name)) {
			$item->name_editor['text'] = $item->name;
		}
		return parent::set_data($item);
	}
	
}

