<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

function local_escreencontrol_enrol_user(&$user) {
	
	global $DB;
	
	$courseid = 2; // TODO into admin settings
	$escreenid = 2; // TODO into admin settings
	$roleid = 5; // (student) TODO into admin settings
	
	$enrol_manual = enrol_get_plugin('manual');
	if (!$enrol_manual) { 
		throw new coding_exception('Can not instantiate enrol_manual'); 
	}
	
	// The user has to be confirmed in order to proceed.
	$user->confirmed = 1;
	$DB->set_field("user", "confirmed", 1, array("id" => $user->id));
	
	$instance = $DB->get_record('enrol', array('courseid' => $courseid, 'enrol' => 'manual'), '*', MUST_EXIST);
	$enrolled = $enrol_manual->enrol_user($instance, $user->id, $roleid);
	
	// Authenticate user (do we have to?)
	authenticate_user_login($user->username, $user->password_plain);
	unset($user->password_plain);
	$user->deleted = 0;
	$user->currentlogin = time();
	$user->lastlogin = time();
	$user->policyagreed = 1;
	complete_user_login($user);
	
	local_escreencontrol_redirect_user($escreenid);
	
}

// This function should return true whether the escreen is completed
// or in process of completion (so as not to redirect user
// to the beginning of it again)
function local_escreencontrol_escreen_completed() {
	global $USER, $SESSION, $CFG, $DB;
	if (!empty($SESSION->escreen)) {
		return true;
	}
	
	$escreenid = 2; // TODO into admin settings
	require_once($CFG->dirroot.'/mod/escreen/lib.php');
	
	$cm = get_coursemodule_from_id('escreen', $escreenid);
	$escreen = $DB->get_record("escreen", array("id"=>$cm->instance));
	if (!$escreen) {
		return true;
	}
	
	$params = array('userid'=>$USER->id, 'escreen'=>$escreen->id);
	return $DB->record_exists('escreen_tracking', $params);
}

function local_escreencontrol_redirect_user() {
	$escreenid = 2; // TODO into admin settings
	redirect(new moodle_url('/mod/escreen/view.php?id=' . $escreenid));
}