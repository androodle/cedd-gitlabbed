<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/*
 * @package    format
 * @subpackage tiles
 * @author     Greg Newton, Androgogic <greg.newton@androgogic.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @copyright  2014 Androgogic, Ltd.
 *
 * TODO: Description goes here
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    // It must be included from a Moodle page.
}

require_once($CFG->dirroot.'/lib/formslib.php');

class chooseimage_form extends moodleform {

    /**
     * Define the form
     */
    public function definition() {
        global $USER, $CFG, $COURSE;

        $mform =& $this->_form;

        $options = array(
            'subdirs' => 0,
            'maxfiles' => 1,
            'maxbytes' => get_max_upload_file_size($CFG->maxbytes),
            'accepted_types' => array('gif', 'jpe', 'jpeg', 'jpg', 'png')/*,
            'return_types' => FILE_INTERNAL*/);

        $mform->addElement('filepicker', 'imagefile',
            get_string('choose_image', 'format_tiles'), '',
            $options
        );
        $mform->addHelpButton('imagefile', 'choose_image', 'format_tiles');

        $mform->addElement('hidden', 'courseid', $this->_customdata['courseid']);
        $mform->setType('courseid', PARAM_INT);

        $mform->addElement('hidden', 'sectionid', $this->_customdata['sectionid']);
        $mform->setType('sectionid', PARAM_INT);

        $this->add_action_buttons(true, get_string('use', 'format_tiles'));
    }

}
