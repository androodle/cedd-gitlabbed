<?php

/**
 * Version details for the Androgogic Tile format
 *
 * @package    format
 * @subpackage tiles
 * @copyright  2012 onwards Androgogic Pty Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2014062601;        // The current plugin version (Date: YYYYMMDDXX)
$plugin->requires  = 2013051400;        // Requires this Moodle version
$plugin->component = 'format_tiles';    // Full name of the plugin (used for diagnostics)
