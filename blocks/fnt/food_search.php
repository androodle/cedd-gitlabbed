<?php

/** 
 * Fnt Block: Search 
 * 
 * @author      Kirill Astashov <kirill@androgogic.com> 
 * @version     08/09/2014 
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Search block_fnt_foods
 * Also provides access to edit and delete functions if user has sufficient permissions
 *  
 **/

//params
$sort   = optional_param('sort', 'date_created', PARAM_TEXT);
$dir    = optional_param('dir', 'ASC', PARAM_ALPHA);
$page   = optional_param('page', 0, PARAM_INT);
$perpage= optional_param('perpage', 50, PARAM_INT);
$search = optional_param('search', '', PARAM_TEXT);
$tab    = optional_param('tab', 'food_search', PARAM_FILE);
// prepare url for paging bar
$PAGE->set_url($PAGE->url, compact('sort', 'dir', 'page','perpage','search','tab')); 
// prepare columns for results table
$columns = array(
    'sort01' => "dateconsumed",
    'sort02' => "timewokeup",
    'sort03' => "timeate",
    "unsort01" => "meal",
    "unsort021" => "food",
    "unsort03" => "beverages",
    "unsort04" => "bevamount",
    "unsort05" => "urgetobinge",
    "unsort06" => "didyoubinge",
    "unsort07" => "overeat",
    "unsort08" => "eventsandfeelings",
    "sort04" => "userid",
    );

$fromdate = 0;
$todate = 0;
if (!empty($_REQUEST['enabledatefilter']) && !empty($_REQUEST['fromdate']) && !empty($_REQUEST['todate'])) {
    $calendartype = \core_calendar\type_factory::get_calendar_instance();
    $gregoriandate = $calendartype->convert_to_gregorian($_REQUEST['fromdate']['year'], $_REQUEST['fromdate']['month'], $_REQUEST['fromdate']['day']);
    $fromtimestamp = make_timestamp($gregoriandate['year'],
                                              $gregoriandate['month'],
                                              $gregoriandate['day'],
                                              0, 0, 0,
                                              99,
                                              true);
    $gregoriandate = $calendartype->convert_to_gregorian($_REQUEST['todate']['year'], $_REQUEST['todate']['month'], $_REQUEST['todate']['day']);
    $totimestamp = make_timestamp($gregoriandate['year'],
                                              $gregoriandate['month'],
                                              $gregoriandate['day'],
                                              0, 0, 0,
                                              99,
                                              true);
    // include the 'to' day as well
    $totimestamp += 24 * 3600;
}



foreach ($columns as $key => $column) {
    $string[$column] = get_string("$column",'block_fnt');
    if ($sort != $column) {
        $columnicon = '';
        $columndir = 'ASC';
    } else {
        $columndir = $dir == 'ASC' ? 'DESC':'ASC';
        $columnicon = $dir == 'ASC' ? 'down':'up';
    }
    if (substr($key, 0, 4) == 'sort') {
        $$column = "<a href='$PAGE->url&amp;dir=$columndir&amp;sort=$column'>$string[$column]</a>";
    } else {
        $$column = $string[$column];
    }
}
//figure out the and clause from what has been submitted
$and = '';
if ($search != '') {
    $and .= " and concat(
    u.firstname,
    u.lastname,
    u.username
    ) like '%$search%'";
}
if (!has_capability('block/fnt:viewotherrecord', $context)) {
    $and .= "AND userid = {$USER->id}";
}
if (!empty($fromtimestamp)) {
    $and .= "
        AND dateconsumed >= $fromtimestamp
        AND dateconsumed <= $totimestamp
    ";
}
$q = "select a.*, u.firstname, u.lastname, u.username
    from {block_fnt_food} a
    LEFT JOIN mdl_user u on a.userid = u.id
    where 1 = 1
    $and
    order by $sort $dir";


//get a page worth of records
$results = $DB->get_records_sql($q,array(),$page*$perpage, $perpage);

// count results
$q = "select COUNT(a.id)
    from {block_fnt_food} a
    LEFT JOIN {user} u on a.userid = u.id
    where 1 = 1 $and";
$result_count = $DB->get_field_sql($q);

if ($download == '') {
    require_once('food_search_form.php');
    $mform = new block_fnt_food_search_form(null, array('sort' => $sort,'dir'=>$dir,'perpage'=>$perpage,'search'=>$search,'tab'=>$currenttab ));
    $mform->display();
    echo '<table width="100%"><tr><td width="50%">';
    echo $result_count . ' ' . get_string('food_plural','block_fnt') . " found" . '<br>';
    echo '</td><td style="text-align:right;">';
    if (has_capability('block/fnt:addrecord', $context)) {
        echo "<a href='index.php?tab=food_new&courseid={$courseid}'>" . get_string('food_new','block_fnt') . "</a>";
    }
    echo '</td></tr></table>';
}

//RESULTS
if (!$results) {
    if($download == ''){
        echo $OUTPUT->heading(get_string('noresults','block_fnt',$search));
    }
} else {
    $table = new html_table();
    $table->head = array (
        $dateconsumed,
        $timewokeup,
        $timeate,
        $meal,
        $food,
        $beverages,
        $bevamount,
        $urgetobinge,
        $didyoubinge,
        $overeat,
        $eventsandfeelings,
        );
    if (has_capability('block/fnt:viewotherrecord', $context)) {
        $table->head[] = $userid;
    }
    $table->head[] = 'Action';
    $table->align = array ("left","left","left","left","left","left","left","left","left","left","left","left","left","left","left","left","left","left","left","left","left","left","left","left","left","left","left",);
    $table->width = "95%";
    $table->size = array("4%","4%","4%","4%","4%","4%","4%","4%","4%","4%","4%","4%","4%","4%","4%","4%","4%","4%","4%","4%","4%","4%","4%","4%","4%","4%","4%",);
    foreach ($results as $result) {
        $view_link = '';//enable if wanted: "<a href='index.php?tab=food_view&id=$result->id'>View</a> ";
        $edit_link = "";
        $delete_link = "<a href='index.php?tab=food_delete&id={$result->id}&courseid={$courseid}' onclick='return confirm(\"Are you sure you want to delete this record?\")'>Delete</a> ";
        /*
        if ((has_capability('block/fnt:addrecord', $context) && $result->userid == $USER->id)
            || has_capability('block/fnt:editotherrecord', $context)) {
                // $edit_link = "<a href='index.php?tab=food_edit&id={$result->id}&courseid={$courseid}'>Edit</a> ";
                $delete_link = "<a href='index.php?tab=food_delete&id={$result->id}&courseid={$courseid}' onclick='return confirm(\"Are you sure you want to delete this record?\")'>Delete</a> ";
        } */
        $result->dateconsumed = userdate($result->dateconsumed, '%a, %B %e, %Y');
        $result->timewokeup = substr($result->timewokeup, 0, 5);
        $result->timeate = substr($result->timeate, 0, 5);
        $result->urgetobinge = ($result->urgetobinge) ? 'Yes' : 'No';
        $result->didyoubinge = ($result->didyoubinge) ? 'Yes' : 'No';
        $result->overeat = ($result->overeat) ? 'Yes' : 'No';
        $username = "{$result->firstname} {$result->lastname} ({$result->username})";
        $arr = array (
            "$result->dateconsumed",
            "$result->timewokeup",
            "$result->timeate",
            "$result->meal",
            "$result->food",
            "$result->beverages",
            "$result->bevamount",
            "$result->urgetobinge",
            "$result->didyoubinge",
            "$result->overeat",
            "$result->eventsandfeelings",
            );
        if (has_capability('block/fnt:viewotherrecord', $context)) {
            $arr[] = $username;
        }
        $arr[] = $view_link . $edit_link . $delete_link;
        $table->data[] = $arr;
    }

}
if(!empty($table)) {
    if ($download != '') {
    //export the table to whatever they asked for
    block_fnt_export_data($download,$table,$tab);
    } else {
        echo html_writer::table($table);
        $pagingbar = new paging_bar($result_count, $page, $perpage,$PAGE->url);
        $pagingbar->pagevar = 'page';
        echo $OUTPUT->render($pagingbar);
        echo 'Download: ';
        $download_report_url = $PAGE->url . "&download=csv&courseid={$courseid}";
        echo html_writer::link($download_report_url, 'csv');
        echo "&nbsp;";
        $download_report_url = $PAGE->url . "&download=excel&courseid={$courseid}";
        echo html_writer::link($download_report_url, 'excel');
    }
}

