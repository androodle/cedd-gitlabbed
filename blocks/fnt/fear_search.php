<?php

/** 
 * Fnt Block: Search 
 * 
 * @author      Kirill Astashov <kirill@androgogic.com> 
 * @version     02/09/2014 
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Search thoughts
 * Also provides access to edit and delete functions if user has sufficient permissions
 *  
 **/

//params
$sort   = optional_param('sort', 'date_created', PARAM_RAW);
$dir    = optional_param('dir', 'ASC', PARAM_ALPHA);
$page   = optional_param('page', 0, PARAM_INT);
$perpage= optional_param('perpage', 20, PARAM_INT); 
$search = optional_param('search', '', PARAM_TEXT);
$tab    = optional_param('tab', 'thought_search', PARAM_FILE);
// prepare url for paging bar
$PAGE->set_url($PAGE->url, compact('sort', 'dir', 'page','perpage','search','tab')); 
// prepare columns for results table
$columns = array(
    "date_created",
    "forbiddenfood",
    "rating",
    "triggeredbinge",
    "noconsequences",
    "userid",
    );

// check sort param
if (!in_array($sort, $columns)) {
    $sort = 'date_created';
}
foreach ($columns as $column) {
    $string[$column] = get_string("$column",'block_fnt');
    if ($sort != $column) {
        $columnicon = '';
        $columndir = 'ASC';
    } else {
        $columndir = $dir == 'ASC' ? 'DESC':'ASC';
        $columnicon = $dir == 'ASC' ? 'down':'up';
    }
    if($column == 'date_created') {
        $$column = "<a href='$PAGE->url&amp;dir=$columndir&amp;sort=$column'>$string[$column]</a>";
    } else {
        $$column = $string[$column];
    }
}

//figure out the and clause from what has been submitted
$search = addslashes($search);
$and = '';
if ($search != '') {
    $and .= " and concat(
    u.firstname,
    u.lastname,
    u.username
    ) like '%$search%'";
}
if (!has_capability('block/fnt:viewotherrecord', $context)) {
    $and .= "AND userid = {$USER->id}";
}
if (!empty($fromtimestamp)) {
    $and .= "
        AND date_created >= $fromtimestamp
        AND date_created <= $totimestamp
    ";
}
$q = "select DISTINCT a.* , CONCAT(u.firstname,' ', u.lastname, ' (' , u.username , ')' ) as userid
    from {block_fnt_fear} a
    LEFT JOIN {user} u on a.userid = u.id
    where 1 = 1
    $and
    order by $sort $dir";

//get a page worth of records
$results = $DB->get_records_sql($q,array(),$page*$perpage, $perpage);

//also get the total number we have of these
$q = "SELECT COUNT(DISTINCT a.id)
    from {block_fnt_fear} a
    LEFT JOIN {user} u on a.userid = u.id
     where 1 = 1 $and";
$result_count = $DB->get_field_sql($q);

if ($download == '') {
    require_once('fear_search_form.php');
    $mform = new block_fnt_fear_search_form(null, array('sort' => $sort,'dir'=>$dir,'perpage'=>$perpage,'search'=>$search,'tab'=>$currenttab ));
    $mform->display();
    echo '<table width="100%"><tr><td width="50%">';
    echo $result_count . ' ' . get_string('fear_plural','block_fnt') . " found" . '<br>';
    echo '</td><td style="text-align:right;">';
    if (has_capability('block/fnt:addrecord', $context)) {
        echo "<a href='index.php?tab=fear_new&courseid={$courseid}'>" . get_string('fear_new','block_fnt') . "</a>";
    }
    echo '</td></tr></table>';
}

//RESULTS
if (!$results) {
    if($download == ''){
        echo $OUTPUT->heading(get_string('noresults','block_fnt',$search));
    }
} else {
    $table = new html_table();
    $table->head = array (
        $date_created,
        $forbiddenfood,
        $rating,
        $triggeredbinge,
        $noconsequences);
    if (has_capability('block/fnt:viewotherrecord', $context)) {
        $table->head[] = $userid;
    }
    $table->head[] = 'Action';
    $table->align = array ("left","left","left","left","left","left","left","left","left","left","left","left","left","left",);
    $table->width = "95%";
    $table->size = array("7%","7%","7%","7%","7%","7%","7%","7%","7%","7%","7%","7%","7%","7%",);
    foreach ($results as $result) {
        $view_link = '';//enable if wanted: "<a href='index.php?tab=thought_view&id=$result->id'>View</a> ";
        $edit_link = "";
        $delete_link = "<a href='index.php?tab=fear_delete&id=$result->id' onclick='return confirm(\"Are you sure you want to delete this item?\")'>Delete</a> ";
        $result->triggeredbinge = ($result->triggeredbinge) ? 'Yes' : 'No';
        $result->noconsequences = ($result->noconsequences) ? 'Yes' : 'No';
        $arr = array (
            userdate("$result->date_created"),
            "$result->forbiddenfood",
            "$result->rating",
            "$result->triggeredbinge",
            "$result->noconsequences");
        if (has_capability('block/fnt:viewotherrecord', $context)) {
            $arr[] = $result->userid;
        }
        $arr[] = $view_link . $edit_link . $delete_link;
        $table->data[] = $arr;
    }
}
if (!empty($table)) {
    if ($download != '') {
        //export the table to whatever they asked for
        block_fnt_export_data($download,$table,$tab);
    } else {
        echo html_writer::table($table);
        $pagingbar = new paging_bar($result_count, $page, $perpage,$PAGE->url);
        $pagingbar->pagevar = 'page';
        echo $OUTPUT->render($pagingbar);
        echo 'Download: ';
        $download_report_url = $PAGE->url . "&download=csv&courseid={$courseid}";
        echo html_writer::link($download_report_url, 'csv');
        echo "&nbsp;";
        $download_report_url = $PAGE->url . "&download=excel&courseid={$courseid}";
        echo html_writer::link($download_report_url, 'excel');
    }
}

