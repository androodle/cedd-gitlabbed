<?php
/** 
 * Fnt Block: Version 
 * 
 * @author      Kirill Astashov <kirill@androgogic.com> 
 * @version     02/09/2014 
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 *  
 **/

$plugin->version = 2014090200;
$plugin->requires = 2010112400; // Indicate we need at least Moodle 2.0

// End of blocks/fnt/version.php