<?php
/** 
 * Fnt Block: Class 
 * 
 * @author      Kirill Astashov <kirill@androgogic.com> 
 * @version     02/09/2014 
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 *  
 **/

require_once($CFG->dirroot . '/blocks/fnt/lib.php');

class block_fnt extends block_base {

	function init(){
		$this->title = get_string('plugintitle','block_fnt');
	} //end function init

	function get_content(){
		global $CFG, $COURSE, $USER, $DB;
        if ($this->content !== null) {
			return $this->content;
		}
        $config = get_config('block_fnt');
		$this->content = new stdClass;
        $this->content->text = '';

        $links = array();
        $enrolments = array(); // cache courseids the user has been confirmed enrolled in
        $notenrolled = array();

        // Food Diary tool link
        if (block_fnt_checkenrolment($config->coursesfood)) {
            $links[] = html_writer::tag('a', get_string('food_new','block_fnt'),
                    array('href' => $CFG->wwwroot.'/blocks/fnt/index.php?tab=food_new&courseid=' . $COURSE->id));
        }
        
        // Thought Diary tool link
        if (block_fnt_checkenrolment($config->coursesthought)) {
            $links[] = html_writer::tag('a', get_string('thought_new','block_fnt'),
                    array('href' => $CFG->wwwroot.'/blocks/fnt/index.php?tab=thought_new&courseid=' . $COURSE->id));
        }

        // Fear of Foods tool link
        if (block_fnt_checkenrolment($config->coursesfear)) {
            $links[] = html_writer::tag('a', get_string('fear_new','block_fnt'),
                    array('href' => $CFG->wwwroot.'/blocks/fnt/index.php?tab=fear_new&courseid='. $COURSE->id));
        }

        // Reports link
        if (sizeof($links)) {
            $links[] = html_writer::tag('a', get_string('reports','block_fnt'),
                    array('href' => $CFG->wwwroot.'/blocks/fnt/index.php?courseid=' . $COURSE->id));
        }

        $this->content->text = implode('<br />', $links);
		return $this->content;

	} //end function get_content

	function has_config(){
        return true;
	} //end function has_config

} //end class block_fnt

// End of blocks/fnt/block_fnt.php