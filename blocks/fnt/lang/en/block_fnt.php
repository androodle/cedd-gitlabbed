<?php
/** 
 * Fnt Block: Language Pack 
 * 
 * @author      Kirill Astashov <kirill@androgogic.com> 
 * @version     02/09/2014 
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 *  
 **/
$string['pluginname'] = 'Food and Thought Diary';
$string['plugintitle'] = 'Food and Thought Diary';
$string['foodnthoughtdiary'] = 'Food and Thought Diary';
//spares
//$string[''] = '';
//$string[''] = '';
//$string[''] = '';
//block_fnt_food items
$string['block_fnt_food'] = 'Food';
$string['dateconsumed'] = 'Date consumed';
$string['timewokeup'] = 'Time I woke up';
$string['timeate'] = 'Time I had something to eat';
$string['meal'] = 'Meal';
$string['food'] = 'Food';
$string['beverages'] = 'Beverages';
$string['bevamount'] = 'Beverage amount';
$string['urgetobinge'] = 'Did you have the urge to binge?';
$string['overeat'] = 'Did you consider it an over-eat';
$string['didyoubinge'] = 'Did you consider it a binge?';
$string['didyoupurge'] = 'Did you purge?';
$string['purgedtimes'] = 'How many times?';
$string['didyouexcercise'] = 'Did you excercise?';
$string['howlongexcercise'] = 'How long did you excercise for?';
$string['timesexcercised'] = 'How many times?';
$string['didtakelaxatives'] = 'Did you take laxatives?';
$string['howmanylaxatives'] = 'How many did you take?';
$string['didyouelse'] = 'Did you do anything else to compensate?';
$string['describeelse'] = 'Please descibe in the following text box';
$string['eventsandfeelings'] = 'Events and feelings';
$string['use'] = 'Use';
$string['created_by'] = 'Created By';
$string['date_created'] = 'Date Created';
$string['modified_by'] = 'Modified By';
$string['date_modified'] = 'Date Modified';
$string['id'] = 'Id';
$string['compensatorybehaviour'] = 'Compensatory Behaviour';

// Permissions
$string['fnt:addinstance'] = 'Add block to course';
$string['fnt:addrecord'] = 'Add/edit records to block';
$string['fnt:delete'] = 'Delete records from block';
$string['fnt:edit'] = 'Edit own records';
$string['fnt:editotherrecord'] = 'Add/delete records on behalf os other users';
$string['fnt:viewotherrecord'] = 'View records of other users';

//common items
$string['datasubmitted'] = 'The data has been submitted';
$string['itemdeleted'] = 'The item has been deleted';
$string['noresults'] = 'There were no results from your search';
$string['block_fnt:edit'] = 'Edit objects within the Food and Thought Diary block';
$string['block_fnt:delete'] = 'Delete objects within the Food and Thought Diary block';
$string['reports'] = 'Reports';
$string['searchuser'] = 'Filter by user';
$string['searchuser_help'] = 'Filter by username, first name or last name';
$string['fromdate'] = 'From';
$string['todate'] = 'To';
$string['filterbydate'] = 'Filter by date';
$string['enabledatefilter'] = 'Enable date filter';
$string['applyfilter'] = 'Apply filter';
$string['to'] = '&nbsp;&nbsp;&nbsp;to&nbsp;&nbsp;&nbsp;';
$string['spacer'] = '&nbsp;&nbsp;&nbsp;&nbsp;';
$string['savechanges'] = '&nbsp;&nbsp;&nbsp;&nbsp;';

// fear of foods
$string['fear_search'] = 'Fear of Foods Diary report';
$string['fear_new'] = 'Add Fear of Foods record';
$string['forbiddenfood'] = 'Forbidden food';
$string['proceedtofearreport'] = 'Proceed to the Fear of Foods Diary report';
$string['fearrating'] = 'Fear rating';
$string['triggeredbinge'] = 'Triggered a binge';
$string['noconsequences'] = 'Can eat appropriate portions of the food without any negative consequence';
$string['fear_plural'] = 'Fear of Food records';


//food items
$string['foodrecordadded'] = 'The Food record has been added';
$string['addanotherfoodrecord'] = 'Add another Food record';
$string['food_search'] = 'Food Diary report';
$string['food_plural'] = 'Food records';
$string['food_new'] = 'Add Food record';
$string['food_edit'] = 'Edit Food Record';
$string['food_search_instructions'] = 'Search by Food, Beverages, Bevamount, Howlongexcercise, Timesexcercised, Describeelse or  Eventsandfeelings';
$string['datetimethought'] = 'Date and time';
$string['promptingevent'] = 'Prompting event';
$string['unhelpthought'] = 'Unhelpful thought';
$string['emotions'] = 'Emotions';
$string['rating'] = 'Rating';
$string['helpthoughts'] = 'My helpful thought';
$string['howifeel'] = 'How I feel now';

//thought items
$string['thought'] = 'Thought';
$string['thought_search'] = 'Thought Diary report';
$string['thought_plural'] = 'Thought records';
$string['thought_new'] = 'Add Thought record';
$string['thought_edit'] = 'Edit Thought record';
$string['thought_search_instructions'] = 'Search by Promptingevent, Unhelpthought, Helpthoughts or  Howifeel';


$string['userid'] = 'User';

// settings
$string['mealslist'] = 'Meals for the Meals select list';
$string['mealslist_help'] = 'Please enter each meal on a separate line';
$string['forbiddenfoodlist'] = 'Foods for the Forbidden Food select list';
$string['forbiddenfoodlist_help'] = 'Please enter each food on a separate line';
$string['coursesfood'] = 'Course ID enrolments for the Food Diary tool';
$string['coursesfood_help'] = 'Please enter course IDs (separated by comma) the user should be enrolled in to see the Food Diary tool.
    The tool will be shown if the user is enrolled in ALL the listed courses.';
$string['coursesthought'] = 'Course ID enrolments for the Thought tool';
$string['coursesthought_help'] = 'Please enter course IDs (separated by comma) the user should be enrolled in to see the Thought tool.
    The tool will be shown if the user is enrolled in ALL the listed courses.';
$string['coursesfear'] = 'Course ID enrolments for the Fear of Foods tool';
$string['coursesfear_help'] = 'Please enter course IDs (separated by comma) the user should be enrolled in to see the Fear of Foods tool.
    The tool will be shown if the user is enrolled in ALL the listed courses.';

$string['accessdenied'] = 'You do not have permissions to access this page.';

$string['selectuser'] = 'Select user to add record for';
$string['proceedtothoughtreport'] = 'Proceed to the Thought Diary report';

// Food checks
$string['understood'] = 'Understood';
$string['proceedfoodreport'] = 'Proceed to the Food Diary report';
$string['check1'] = 'Don’t give up! The first step of a healthy eating pattern is to eat regularly throughout the day.
    Remember the 3 hour rule - you should eat something every 3 hours. At first, it doesn’t matter as much what you eat,
    the most important thing is that you establish the pattern of eating something every 3 hours. To make regular eating
    easier, choose something that you feel is safe to eat, and that you are fairly sure won’t trigger a binge.';
$string['check2'] = 'Keep at it. I can see that there are times across the day you are observing the 3 hour rule.
    Work on extending this throughout the day. At first, it doesn’t matter as much what you eat, the most important
    thing is that you establish the pattern of eating something every 3 hours. To make regular eating easier,
    choose something that you feel is safe to eat, and that you are fairly sure won’t trigger a binge.';
$string['check3'] = 'Don’t give up! The first step of a healthy eating pattern is to eat regularly throughout the day.
    In the early stages of recovery it is important that you establish the pattern of eating something every 3 hours.
    Remember that going without food biologically drives you to binge. With regular eating over time, your cravings
    and urges to binge or overeat will be reduced, and your metabolism will be kick-started into burning more calories.
    If you are under-eating or restricting your food intake, I encourage you to spend more time working on eating
    2 or 3 planned meals and 2 or 3 planned snacks a day. Remember – if you are struggling, choose something that you
    feel you can safely eat and that won’t trigger a binge. It can also help to plan your meals and snacks ahead
    of time so you are prepared.';
$string['check4'] = 'Well done! It looks like you are slowly getting the hang of eating regularly throughout the day.
    You might start to notice your cravings and frequency of binge eating has reduced. Keep at it. Eating every 3 hours
    and not in between takes time to accomplish and maintain, but it leads to great rewards.';
$string['check5'] = 'Fantastic work! It looks like you are doing very well at eating regularly throughout the day.
    Keep up the great work!';
$string['check6'] = 'Remember to have your first meal or snack within 1 hour of waking. You go for at least 6 hours
    without food while you sleep. To make the body feel safe so it doesn’t have the urge to binge remember to feed it
    soon after waking.';
$string['check7'] = 'Great work! You went the whole day without having the urge to binge or bingeing. You are closer
    to breaking your cycle of binge eating! Thank you for your hard work.';
$string['check8'] = 'Well done! You experienced an urge to binge eat, but did not binge. You should be proud
    of yourself. It must have been hard not to act on your urge. Each time you control your urge to binge you are
    one step closer to breaking your cycle of binge eating! Keep up the good work!';
$string['check9'] = 'I have noticed that you recorded an urge to binge and binge behaviours. Well done for noticing
    your urge. Next time, try to surf the urge and see if you can get through without bingeing.';
$string['check10'] = 'I understand that the idea of changing the nature of your relationship with food can be scary,
    overwhelming, and hard work. The road to recovery is not perfect, but we will take it slowly and solve problems
    as they come up. Remember to work on eating every 3 hours, and try to pay more attention to the events, thoughts,
    and emotions that you experience when you eat. Don’t forget to monitor your food intake using your food diary,
    and don’t be discouraged from focusing on your goal of overcoming your binge eating.';
$string['check11'] = 'Great work! You went the whole day without doing any behaviour to compensate for the food you ate.
    You are closer to breaking your cycle of binge eating! Thank you for your hard work.';
$string['check12'] = 'Well done! You experienced an urge to binge eat, but did not binge or do any compensatory
    behaviour. You should be proud of yourself. It must have been hard not to act on your urge to binge.
    Each time you control your urge to binge you are one step closer to breaking your cycle of binge eating!
    Keep up the good work!';
$string['check13'] = 'I have noticed that you recorded an urge to binge and binge behaviours. Well done for noticing
        your urge. Next time, try to go through the ride of the urge and see if you can get through without bingeing.';