<?php

/** 
 * Fnt Block: Edit object 
 * 
 * @author      Kirill Astashov <kirill@androgogic.com> 
 * @version     02/09/2014 
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Edit one of the thoughts
 *  
 **/

global $OUTPUT;
require_capability('block/fnt:edit', $context);
require_once('thought_edit_form.php');
$id = required_param('id', PARAM_INT);
$q = "select DISTINCT a.* , CONCAT(mdl_user.firstname,' ',mdl_user.lastname) as userid 
from mdl_block_fnt_thought a 
LEFT JOIN mdl_user  on a.userid = mdl_user.id
where a.id = $id ";
$thought = $DB->get_record_sql($q);
$mform = new thought_edit_form();
if ($data = $mform->get_data()){
$data->id = $id;
$data->modified_by = $USER->id;
$data->date_modified = date('Y-m-d H:i:s');
$data->promptingevent = format_text($data->promptingevent['text'], $data->promptingevent['format']);
$data->unhelpthought = format_text($data->unhelpthought['text'], $data->unhelpthought['format']);
$data->helpthoughts = format_text($data->helpthoughts['text'], $data->helpthoughts['format']);
$data->howifeel = format_text($data->howifeel['text'], $data->howifeel['format']);
$DB->update_record('block_fnt_thought',$data);
echo $OUTPUT->notification(get_string('datasubmitted','block_fnt'), 'notifysuccess');
}
else{
echo $OUTPUT->heading(get_string('thought_edit', 'block_fnt'));
$mform->display();
}

