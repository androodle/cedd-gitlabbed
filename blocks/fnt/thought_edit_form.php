<?php

/** 
 * Fnt Block: Edit form 
 * 
 * @author      Kirill Astashov <kirill@androgogic.com> 
 * @version     02/09/2014 
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Provides edit form for the object.
 * This is used by both new and edit pages
 *  
 **/

if (!defined('MOODLE_INTERNAL')) {
    die ('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');
class thought_edit_form extends moodleform {
    protected $thought;
    function definition() {
        global $USER, $courseid, $DB, $PAGE, $emotionsarr, $ratings;
        $mform =& $this->_form;
        $coursecontext = context_course::instance($courseid);
        $id = optional_param('id', 0, PARAM_INT);
        if ($id) {
            $q = "select DISTINCT a.* , CONCAT(u.firstname,' ',u.lastname, ' (', u.username, ')') as uname
            from {block_fnt_thought} a
            LEFT JOIN {user} u on a.userid = u.id
            where a.id = $id";
            $thought = $DB->get_record_sql($q);
        } else {
            $thought = $this->_customdata['$thought']; // this contains the data of this form
        }
        $tab = 'thought_new'; // from whence we were called
        if (!empty($thought->id)) {
            $tab = 'thought_edit';
        }
        $mform->addElement('html','<div>');

        $editoroptions = array('rows' => 5, 'cols' => 80);

        //datetimethought
        $mform->addElement('date_time_selector', 'datetimethought', get_string('datetimethought','block_fnt'));
        $mform->addRule('datetimethought', get_string('required'), 'required', null, 'server');

        //promptingevent
        $mform->addElement('textarea', 'promptingevent', get_string('promptingevent','block_fnt'), $editoroptions);
        $mform->setType('promptingevent', PARAM_CLEANHTML);
        $mform->addRule('promptingevent', get_string('required'), 'required', null, 'server');

        //unhelpthought
        $mform->addElement('textarea', 'unhelpthought', get_string('unhelpthought','block_fnt'), $editoroptions);
        $mform->setType('unhelpthought', PARAM_CLEANHTML);
        $mform->addRule('unhelpthought', get_string('required'), 'required', null, 'server');

        //emotions & rating
        // $mform->addElement('html','<div class="fitem"><div class="fitemtitle"><label>' . get_string('emotions', 'block_fnt') . '</label></div></div>');
        $group = array();
        $group[] =& $mform->createElement('static', 'tbegin', null, '<table border=0 cellpadding=5 valign=top>');
        foreach ($emotionsarr as $emotion) {
            $group[] =& $mform->createElement('static', strtolower($emotion) . '_trbegin', null, '<tr><td width=150 align=left valign=top>');
            $group[] =& $mform->createElement('checkbox', strtolower($emotion) . '_yesno', $emotion);
            $group[] =& $mform->createElement('static', strtolower($emotion) . '_emotitle', null, $emotion);
            $group[] =& $mform->createElement('static', strtolower($emotion) . '_trcont', null, '</td><td valign=top>');
            $group[] =& $mform->createElement('select', strtolower($emotion) . '_rating', '', $ratings);
            $group[] =& $mform->createElement('static', strtolower($emotion) . '_trend', null, '</td></tr>');
        }
        $group[] =& $mform->createElement('static', 'tend', null, '</table>');
        $mform->addGroup($group, 'emotionsratings', get_string('emotions', 'block_fnt'), array(' '), false);

        //helpthoughts
        $mform->addElement('textarea', 'helpthoughts', get_string('helpthoughts','block_fnt'), $editoroptions);
        $mform->setType('helpthoughts', PARAM_CLEANHTML);
        $mform->addRule('helpthoughts', get_string('required'), 'required', null, 'server');

        //howifeel
        $mform->addElement('textarea', 'howifeel', get_string('howifeel','block_fnt'), $editoroptions);
        $mform->setType('howifeel', PARAM_CLEANHTML);
        $mform->addRule('howifeel', get_string('required'), 'required', null, 'server');

        //userid
        if (has_capability('block/fnt:editotherrecord', $coursecontext)) {
            $users = get_enrolled_users($coursecontext, '', 0, 'u.*', 'u.firstname ASC, u.lastname ASC, u.username ASC');
            $usersarr = array();
            foreach ($users as $user) {
                $usersarr[$user->id] = "$user->firstname $user->lastname ({$user->username})";
            }
            $select = $mform->addElement('searchableselector', 'userid', get_string('selectuser', 'block_fnt'), $usersarr);
            $select->setMultiple(false);
        }

        //set values if we are in edit mode
        if (!empty($thought->id) && $id) {
            $mform->setConstant('datetimethought', strtotime($thought->datetimethought));
            $promptingevent['text'] = $thought->promptingevent;
            $promptingevent['format'] = 1;
            $mform->setDefault('promptingevent', $promptingevent);
            $unhelpthought['text'] = $thought->unhelpthought;
            $unhelpthought['format'] = 1;
            $mform->setDefault('unhelpthought', $unhelpthought);
            $helpthoughts['text'] = $thought->helpthoughts;
            $helpthoughts['format'] = 1;
            $mform->setDefault('helpthoughts', $helpthoughts);
            $howifeel['text'] = $thought->howifeel;
            $howifeel['format'] = 1;
            $mform->setDefault('howifeel', $howifeel);
            $mform->setConstant('userid', $thought->userid);
        }
        //hiddens
        $mform->addElement('hidden','tab',$tab);
        $mform->setType('tab', PARAM_CLEANHTML);
        $mform->addElement('hidden', 'courseid', $courseid);
        $mform->setType('courseid', PARAM_INT);
        if ($id) {
            $mform->addElement('hidden','id',$_REQUEST['id']);
            $mform->setType('id', PARAM_INT);
        }
        $this->add_action_buttons(true);
        $mform->addElement('html','</div>');
    }
}
