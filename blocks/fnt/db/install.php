<?php
/** 
 * Fnt Block: Install DB scripts 
 * 
 * @author      Kirill Astashov <kirill@androgogic.com> 
 * @version     02/09/2014 
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 *  
 **/
function xmldb_block_fnt_install() {
    global $DB;
    $result = true;
    // create tables for the block
    if (!$DB->get_manager()->table_exists('block_fnt_food')) {
        $sql = "CREATE TABLE `mdl_block_fnt_food` (
                `id` bigint(11) NOT NULL AUTO_INCREMENT,
                `courseid` bigint(11) NOT NULL,
                `dateconsumed` bigint(11) NOT NULL,
                `timewokeup` time,
                `timeate` time NOT NULL DEFAULT '00:00',
                `meal` varchar(40) NOT NULL DEFAULT '',
                `food` text NULL,
                `beverages` text NULL,
                `bevamount` varchar(40) NOT NULL DEFAULT '',
                `urgetobinge` tinyint(4) NOT NULL DEFAULT 0,
                `overeat` tinyint(4) NOT NULL DEFAULT 0,
                `didyoubinge` tinyint(4) NOT NULL DEFAULT 0,
                `didyoupurge` tinyint(4) NOT NULL DEFAULT 0,
                `purgedtimes` smallint(6) NOT NULL DEFAULT 0,
                `didyouexcercise` tinyint(4) NOT NULL DEFAULT 0,
                `howlongexcercise` varchar(40) NOT NULL DEFAULT '',
                `timesexcercised` varchar(40) NOT NULL DEFAULT '',
                `didtakelaxatives` tinyint(4) NOT NULL DEFAULT 0,
                `howmanylaxatives` varchar(40) NOT NULL DEFAULT '',
                `didyouelse` tinyint(4) NOT NULL DEFAULT 0,
                `describeelse` text NULL,
                `eventsandfeelings` text NULL,
                `userid` int(10) NOT NULL,
                `created_by` bigint(10) NOT NULL COMMENT 'relates to user table',
                `date_created` bigint(11) NOT NULL,
                `modified_by` bigint(10) DEFAULT NULL COMMENT 'relates to user table',
                `date_modified` bigint(11) DEFAULT NULL,
                PRIMARY KEY (`id`),
                KEY `courseid` (`courseid`),
                KEY `userid` (`userid`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
        $DB->execute($sql);
    }
    if (!$DB->get_manager()->table_exists('block_fnt_thought')) {
        $sql = "CREATE TABLE `mdl_block_fnt_thought` (
                `id` bigint(11) NOT NULL AUTO_INCREMENT,
                `courseid` bigint(11) NOT NULL,
                `datetimethought` bigint(11) NOT NULL,
                `promptingevent` text NULL,
                `unhelpthought` text NULL,
                `emotions` text NULL,
                `rating` text NULL,
                `helpthoughts` text NULL,
                `howifeel` text NULL,
                `userid` int(10) NOT NULL,
                `created_by` bigint(10) NOT NULL COMMENT 'relates to user table',
                `date_created` bigint(11) NOT NULL,
                `modified_by` bigint(10) DEFAULT NULL COMMENT 'relates to user table',
                `date_modified` bigint(11) DEFAULT NULL,
                PRIMARY KEY (`id`),
                KEY `courseid` (`courseid`),
                KEY `userid` (`userid`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
        $DB->execute($sql);
    }
    if (!$DB->get_manager()->table_exists('block_fnt_fear')) {
        $sql = "CREATE TABLE `mdl_block_fnt_fear` (
                `id` bigint(11) NOT NULL AUTO_INCREMENT,
                `courseid` bigint(11) NOT NULL,
                `forbiddenfood` varchar(255) NULL,
                `rating` tinyint(4) NULL,
                `triggeredbinge` tinyint(4) NULL,
                `noconsequences` tinyint(4) NULL,
                `userid` int(10) NOT NULL,
                `created_by` bigint(10) NOT NULL COMMENT 'relates to user table',
                `date_created` bigint(11) NOT NULL,
                `modified_by` bigint(10) DEFAULT NULL COMMENT 'relates to user table',
                `date_modified` bigint(11) DEFAULT NULL,
                PRIMARY KEY (`id`),
                KEY `courseid` (`courseid`),
                KEY `userid` (`userid`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
        $DB->execute($sql);
    }
    return $result;
}
