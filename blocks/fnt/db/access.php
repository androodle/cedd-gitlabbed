<?php
/** 
 * Fnt Block: Permissions 
 * 
 * @author      Kirill Astashov <kirill@androgogic.com> 
 * @version     02/09/2014 
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 *  
 **/
$capabilities = array(
    'block/fnt:addinstance' => array(
        'captype' => 'write',
        'contextlevel' => CONTEXT_COURSE,
        'archetypes' => array(
        'manager' => CAP_ALLOW,
        )
    ),
    // capability to add/edit/delete records
    'block/fnt:addrecord' => array(
        'captype' => 'write',
        'contextlevel' => CONTEXT_COURSE,
        'archetypes' => array(
        'manager' => CAP_ALLOW,
        'student' => CAP_ALLOW,
        )
    ),
    // capability to add/edit/delete others' records
    'block/fnt:editotherrecord' => array(
        'captype' => 'write',
        'contextlevel' => CONTEXT_COURSE,
        'archetypes' => array(
        'manager' => CAP_ALLOW,
        )
    ),
    // capability to view others' records/reports
    'block/fnt:viewotherrecord' => array(
        'captype' => 'read',
        'contextlevel' => CONTEXT_COURSE,
        'archetypes' => array(
        'manager' => CAP_ALLOW,
        )
    ),
);
// End of blocks/fnt/db/access.php
