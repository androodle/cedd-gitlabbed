<?php

/** 
 * Fnt Block: Edit form 
 * 
 * @author      Kirill Astashov <kirill@androgogic.com> 
 * @version     08/09/2014 
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Provides edit form for the object.
 * This is used by both new and edit pages
 *  
 **/

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once($CFG->libdir . '/formslib.php');
require_once(dirname(__FILE__) . '/timeselector.php');

class block_fnt_food_edit_form extends moodleform {
    protected $block_fnt_food;

    function definition() {
        global $USER, $DB, $PAGE, $courseid;
        $mform =& $this->_form;
        $id = optional_param('id', 0, PARAM_INT);
        $context = context_system::instance();
        if (isset($_REQUEST['id'])) {
            $q = "select DISTINCT a.*
            from {block_fnt_food} a
            where a.id = {$_REQUEST['id']} ";
            $block_fnt_food = $DB->get_record_sql($q);
        } else {
            $block_fnt_food = $this->_customdata['$block_fnt_food']; // this contains the data of this form
        }

        // from whence we were called
        if (!empty($block_fnt_food->id)) {
            $tab = 'food_edit';
        } else {
            $tab = 'food_new';
        }

        $editoroptions = array('rows' => 5, 'cols' => 80);
        $config = get_config('block_fnt');

        $mform->addElement('html','<div>');

        //dateconsumed
        $mform->addElement('date_selector', 'dateconsumed', get_string('dateconsumed','block_fnt'));
        $mform->addRule('dateconsumed', get_string('required'), 'required', null, 'server');

        //timewokeup
        $mform->addElement('time_selector', 'timewokeup', get_string('timewokeup','block_fnt'), array('size'=>50));
        $mform->setType('timewokeup', PARAM_TEXT);
        
        //timeate
        $mform->addElement('time_selector', 'timeate', get_string('timeate','block_fnt'), array('size'=>50));
        $mform->setType('timeate', PARAM_TEXT);

        //meal
        $config->mealslist = trim($config->mealslist);
        $mealslistarr = explode("\n", $config->mealslist);
        $mealslist = array();
        foreach ($mealslistarr as &$meal) {
            $meal = trim($meal);
            if (!empty($meal)) {
                $mealslist[$meal] = $meal;
            }
        }
        $mform->addElement('select', 'meal', get_string('meal','block_fnt'), $mealslist);
        $mform->addRule('meal', get_string('required'), 'required', null, 'server');
        $mform->addRule('meal', 'Maximum 50 characters', 'maxlength', 50, 'client');
        $mform->setType('meal', PARAM_TEXT);

        //food
        $mform->addElement('textarea', 'food', get_string('food','block_fnt'), $editoroptions);
        $mform->setType('food', PARAM_CLEANHTML);
        $mform->addRule('food', get_string('required'), 'required', null, 'server');
        $mform->setType('food', PARAM_TEXT);

        //beverages
        $mform->addElement('textarea', 'beverages', get_string('beverages','block_fnt'), $editoroptions);
        $mform->setType('beverages', PARAM_CLEANHTML);
        $mform->addRule('beverages', get_string('required'), 'required', null, 'server');
        $mform->setType('beverages', PARAM_TEXT);

        //bevamount
        $mform->addElement('text', 'bevamount', get_string('bevamount','block_fnt'), array('size'=>40));
        $mform->addRule('bevamount', get_string('required'), 'required', null, 'server');
        $mform->addRule('bevamount', 'Maximum 40 characters', 'maxlength', 40, 'client');
        $mform->setType('bevamount', PARAM_TEXT);

        //urgetobinge
        $mform->addElement('selectyesno', 'urgetobinge', get_string('urgetobinge','block_fnt'));

        //didyoubinge
        $mform->addElement('selectyesno', 'didyoubinge', get_string('didyoubinge','block_fnt'));
        $mform->disabledIf('didyoubinge', 'urgetobinge', 'neq', 1);

        //overeat
        $mform->addElement('selectyesno', 'overeat', get_string('overeat','block_fnt'));
        $mform->disabledIf('overeat', 'urgetobinge', 'neq', 1);

        $mform->addElement('header', 'cb', get_string('compensatorybehaviour', 'block_fnt'));

        //didyoupurge
        $mform->addElement('selectyesno', 'didyoupurge', get_string('didyoupurge','block_fnt'));
        $mform->setType('didyoupurge', PARAM_INT);

        //purgedtimes
        $mform->addElement('text', 'purgedtimes', get_string('purgedtimes','block_fnt'), array('size'=>4));
        $mform->addRule('purgedtimes', 'Maximum 4 characters', 'maxlength', 4, 'client');
        $mform->disabledIf('purgedtimes', 'didyoupurge', 'neq', 1);
        $mform->setType('purgedtimes', PARAM_INT);

        //didyouexcercise
        $mform->addElement('selectyesno', 'didyouexcercise', get_string('didyouexcercise','block_fnt'));

        //howlongexcercise
        $mform->addElement('text', 'howlongexcercise', get_string('howlongexcercise','block_fnt'), array('size'=>40));
        $mform->addRule('howlongexcercise', 'Maximum 40 characters', 'maxlength', 40, 'client');
        $mform->setType('howlongexcercise', PARAM_TEXT);
        $mform->disabledIf('howlongexcercise', 'didyouexcercise', 'neq', 1);

        //timesexcercised
        $mform->addElement('text', 'timesexcercised', get_string('timesexcercised','block_fnt'), array('size'=>4));
        $mform->addRule('timesexcercised', 'Maximum 4 characters', 'maxlength', 4, 'client');
        $mform->setType('timesexcercised', PARAM_INT);
        $mform->disabledIf('timesexcercised', 'didyouexcercise', 'neq', 1);

        //didtakelaxatives
        $mform->addElement('selectyesno', 'didtakelaxatives', get_string('didtakelaxatives','block_fnt'));

        //howmanylaxatives
        $mform->addElement('text', 'howmanylaxatives', get_string('howmanylaxatives','block_fnt'), array('size'=>40));
        $mform->addRule('howmanylaxatives', 'Maximum 40 characters', 'maxlength', 40, 'client');
        $mform->setType('howmanylaxatives', PARAM_TEXT);
        $mform->disabledIf('howmanylaxatives', 'didtakelaxatives', 'neq', 1);

        //didyouelse
        $mform->addElement('selectyesno', 'didyouelse', get_string('didyouelse','block_fnt'));

        //describeelse
        $mform->addElement('textarea', 'describeelse', get_string('describeelse','block_fnt'), $editoroptions);
        $mform->setType('describeelse', PARAM_CLEANHTML);
        $mform->disabledIf('describeelse', 'didyouelse', 'neq', 1);

        //eventsandfeelings
        $mform->addElement('textarea', 'eventsandfeelings', get_string('eventsandfeelings','block_fnt'), $editoroptions);
        $mform->setType('eventsandfeelings', PARAM_CLEANHTML);
        $mform->addRule('eventsandfeelings', get_string('required'), 'required', null, 'server');

        //userid
        $coursecontext = context_course::instance($courseid, MUST_EXIST);
        if (has_capability('block/fnt:editotherrecord', $coursecontext)) {
            $users = get_enrolled_users($coursecontext, '', 0, 'u.*', 'u.firstname ASC, u.lastname ASC, u.username ASC');
            $usersarr = array();
            foreach ($users as $user) {
                $usersarr[$user->id] = "$user->firstname $user->lastname ({$user->username})";
            }
            $select = $mform->addElement('searchableselector', 'userid', get_string('selectuser', 'block_fnt'), $usersarr);
            $select->setMultiple(false);
        }
        //set values if we are in edit mode
        if (!empty($block_fnt_food->id) && isset($_GET['id'])) {
            $mform->setConstant('dateconsumed', strtotime($block_fnt_food->dateconsumed));
            $mform->setConstant('timewokeup', $block_fnt_food->timewokeup);
            $mform->setConstant('timeate', $block_fnt_food->timeate);
            $mform->setConstant('meal', $block_fnt_food->meal);
            $food['text'] = $block_fnt_food->food;
            $food['format'] = 1;
            $mform->setDefault('food', $food);
            $beverages['text'] = $block_fnt_food->beverages;
            $beverages['format'] = 1;
            $mform->setDefault('beverages', $beverages);
            $mform->setConstant('bevamount', $block_fnt_food->bevamount);
            $mform->setConstant('urgetobinge', $block_fnt_food->urgetobinge);
            $mform->setConstant('overeat', $block_fnt_food->overeat);
            $mform->setConstant('didyoubinge', $block_fnt_food->didyoubinge);
            $mform->setConstant('didyoupurge', $block_fnt_food->didyoupurge);
            $mform->setConstant('purgedtimes', $block_fnt_food->purgedtimes);
            $mform->setConstant('didyouexcercise', $block_fnt_food->didyouexcercise);
            $mform->setConstant('howlongexcercise', $block_fnt_food->howlongexcercise);
            $mform->setConstant('timesexcercised', $block_fnt_food->timesexcercised);
            $mform->setConstant('didtakelaxatives', $block_fnt_food->didtakelaxatives);
            $mform->setConstant('howmanylaxatives', $block_fnt_food->howmanylaxatives);
            $mform->setConstant('didyouelse', $block_fnt_food->didyouelse);
            $describeelse['text'] = $block_fnt_food->describeelse;
            $describeelse['format'] = 1;
            $mform->setDefault('describeelse', $describeelse);
            $eventsandfeelings['text'] = $block_fnt_food->eventsandfeelings;
            $eventsandfeelings['format'] = 1;
            $mform->setDefault('eventsandfeelings', $eventsandfeelings);
            $mform->setConstant('userid', $block_fnt_food->userid);
        }
        //hiddens
        $mform->addElement('hidden','tab',$tab);
        $mform->setType('tab', PARAM_TEXT);
        $mform->addElement('hidden','courseid',$courseid);
        $mform->setType('courseid', PARAM_INT);
        if (isset($_REQUEST['id'])) {
            $mform->addElement('hidden','id',$_REQUEST['id']);
            $mform->setType('id', PARAM_INT);
        } else if(isset($id)) {
            $mform->addElement('hidden', 'id', $id);
            $mform->setType('id', PARAM_INT);
        }
        $this->add_action_buttons(true);
        $mform->addElement('html','</div>');
    }
}
