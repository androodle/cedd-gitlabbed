<?php
/** 
 * Fnt Block: Index 
 * 
 * @author      Kirill Astashov <kirill@androgogic.com> 
 * @version     02/09/2014 
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 *  
 **/

require_once('../../config.php');
require_once('lib.php');
global $CFG,$USER,$DB,$PAGE, $OUTPUT;
//expected params

$currenttab = optional_param('tab', 'food_search', PARAM_FILE);
$download = optional_param('download','',PARAM_RAW);
$courseid = required_param('courseid', PARAM_INT);
$cancel = optional_param('cancel','',PARAM_TEXT);

$context = context_course::instance($courseid);
$PAGE->set_context($context);
$PAGE->set_url("$CFG->wwwroot/blocks/fnt/index.php", array('tab'=>$currenttab, 'courseid' => $courseid));
$PAGE->set_title(get_string($currenttab, 'block_fnt'));
$PAGE->set_heading(get_string('plugintitle', 'block_fnt'));
$PAGE->set_pagelayout('standard');
// $PAGE->set_pagelayout('general');
$course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
// $PAGE->navbar->add($course->shortname, "$CFG->wwwroot/course/view.php?id={$courseid}");
$navigation = $PAGE->navbar->add(get_string('foodnthoughtdiary','block_fnt'), "$CFG->wwwroot/blocks/fnt/index.php?courseid={$courseid}");
require_login($course);
$config = get_config('block_fnt');

// Checking if a form has been cancelled (checking here so as to do it before start of page output).
if (!empty($cancel)) {
    $tabarr = explode('_', $currenttab);
    $tabstr = $tabarr[0] . '_search';
    redirect(new moodle_url('/blocks/fnt/index.php', array('tab' => $tabstr, 'courseid' => $courseid)));
    exit;
}

if ($download == '') {
    echo $OUTPUT->header();
    //show tabs
    include('tabs.php');
}

$emotionsarr = array('Happy','Sad', 'Angry', 'Resentful', 'Guilty', 'Pain', 'Depressed', 'Anxious', 'Stressed',
    'Love', 'Joy', 'Lonely', 'Shame', 'Embarrassed', 'Gratitude', 'Frustrated', 'Disgust');
$ratings = array(
    '1' => '1 (Weakest)',
    '2' => '2',
    '3' => '3',
    '4' => '4',
    '5' => '5',
    '6' => '6',
    '7' => '7',
    '8' => '8',
    '9' => '9',
    '10' => '10 (Strongest)',
);
// Fear rating
$fearrating = array(
    '1' => '1 (least feared)',
    '2' => '2',
    '3' => '3',
    '4' => '4',
    '5' => '5',
    '6' => '6',
    '7' => '7',
    '8' => '8',
    '9' => '9',
    '10' => '10 (most feared)',
);

// Check if the user is enrolled in any of courses (see the plugin global settings page) required to access the tab
if (strpos($currenttab, 'food') === 0) {
    block_fnt_require_enrolment($config->coursesfood, $courseid);
}
if (strpos($currenttab, 'thought') === 0) {
    block_fnt_require_enrolment($config->coursesthought, $courseid);
}
if (strpos($currenttab, 'fear') === 0) {
    block_fnt_require_enrolment($config->coursesfear, $courseid);
}

// include current page
include $currenttab.'.php';
if ($download == '') {
    echo $OUTPUT->footer();
}


// End of blocks/fnt/index.php