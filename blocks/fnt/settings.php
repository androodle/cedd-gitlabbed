<?php


/** 
 * Fnt Block: Settings 
 * 
 * @author      Kirill Astashov <kirill@androgogic.com> 
 * @version     02/09/2014 
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 *  
 **/



defined('MOODLE_INTERNAL') || die(); 



if ($ADMIN->fulltree) {
//example text field (INT). CHANGE MY NAME!
//$settings->add(new admin_setting_configtext('block_fnt/run_cron_hours', get_string('cron_hours', 'block_fnt'),
//               get_string('cron_hours_explanation', 'block_fnt'), '', PARAM_INT));
//example text field (EMAIL ADDRESS). CHANGE MY NAME!
//$settings->add(new admin_setting_configtext('block_fnt/it_email', get_string('it_email', 'block_fnt'),
//               get_string('it_email_explanation', 'block_fnt'), '', PARAM_EMAIL));

$settings->add(new admin_setting_configtextarea('block_fnt/mealslist', get_string('mealslist', 'block_fnt'),
                   get_string('mealslist_help', 'block_fnt'), '', PARAM_TEXT));

$settings->add(new admin_setting_configtextarea('block_fnt/forbiddenfoodlist', get_string('forbiddenfoodlist', 'block_fnt'),
                   get_string('forbiddenfoodlist_help', 'block_fnt'), '', PARAM_TEXT));

$settings->add(new admin_setting_configtext('block_fnt/coursesfood', get_string('coursesfood', 'block_fnt'),
                   get_string('coursesfood_help', 'block_fnt'), '', PARAM_TEXT));

$settings->add(new admin_setting_configtext('block_fnt/coursesthought', get_string('coursesthought', 'block_fnt'),
                   get_string('coursesthought_help', 'block_fnt'), '', PARAM_TEXT));

$settings->add(new admin_setting_configtext('block_fnt/coursesfear', get_string('coursesfear', 'block_fnt'),
                   get_string('coursesfear_help', 'block_fnt'), '', PARAM_TEXT));

}




// End of blocks/fnt/settings.php
