<?php

/** 
 * Fnt Block: Search form 
 * 
 * @author      Kirill Astashov <kirill@androgogic.com> 
 * @version     02/09/2014 
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Provides search form for the object.
 * This is used by search page
 *  
 **/

if (!defined('MOODLE_INTERNAL')) {
die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');
class thought_search_form extends moodleform {
    function definition() {
        global $DB, $courseid, $context;
        $mform =& $this->_form;
        foreach ($this->_customdata as $custom_key=>$custom_value) {
            $$custom_key = $custom_value;
        }

        $mform->addElement('html','<div>');
        $mform->addElement('checkbox', 'enabledatefilter', get_string('enabledatefilter', 'block_fnt'));
        $mform->addElement('date_selector', 'fromdate', get_string('fromdate','block_fnt'));
        $mform->addElement('date_selector', 'todate', get_string('todate','block_fnt'));
        $mform->disabledIf('fromdate', 'enabledatefilter', 'notchecked');
        $mform->disabledIf('todate', 'enabledatefilter', 'notchecked');
        if (has_capability('block/fnt:viewotherrecord', $context)) {
            $mform->addElement('text', 'search', get_string('searchuser', 'block_fnt'), get_string('searchuser_help', 'block_fnt'));
        }
        $mform->setType('search', PARAM_TEXT);

        //hiddens
        $mform->addElement('hidden','tab',$tab);
        $mform->setType('tab', PARAM_TEXT);
        $mform->addElement('hidden','sort',$sort);
        $mform->setType('sort', PARAM_TEXT);
        $mform->addElement('hidden','dir',$dir);
        $mform->setType('dir', PARAM_TEXT);
        $mform->addElement('hidden','perpage',$perpage);
        $mform->setType('perpage', PARAM_TEXT);
        $mform->addElement('hidden','courseid',$courseid);
        $mform->setType('courseid', PARAM_INT);
        //button
        $mform->addElement('submit', 'submit', get_string('applyfilter', 'block_fnt'));
    }
}
