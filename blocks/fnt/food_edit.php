<?php

/** 
 * Fnt Block: Edit object 
 * 
 * @author      Kirill Astashov <kirill@androgogic.com> 
 * @version     08/09/2014 
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Edit one of the block_fnt_foods
 *  
 **/

global $OUTPUT;
require_capability('block/fnt:edit', $context);
require_once('food_edit_form.php');
$id = required_param('id', PARAM_INT);
$q = "select DISTINCT a.* , CONCAT(mdl_user.firstname,' ',mdl_user.lastname) as userid 
from {block_fnt_food} a
LEFT JOIN mdl_user  on a.userid = mdl_user.id
where a.id = $id ";
$block_fnt_food = $DB->get_record_sql($q);
$mform = new block_fnt_food_edit_form();
if ($data = $mform->get_data()){
$data->id = $id;
$data->modified_by = $USER->id;
$data->date_modified = date('Y-m-d H:i:s');
$data->food = format_text($data->food['text'], $data->food['format']);
$data->beverages = format_text($data->beverages['text'], $data->beverages['format']);
$data->describeelse = format_text($data->describeelse['text'], $data->describeelse['format']);
$data->eventsandfeelings = format_text($data->eventsandfeelings['text'], $data->eventsandfeelings['format']);
$DB->update_record('block_fnt_food',$data);
echo $OUTPUT->notification(get_string('datasubmitted','block_fnt'), 'notifysuccess');
}
else{
echo $OUTPUT->heading(get_string('food_edit', 'block_fnt'));
$mform->display();
}

