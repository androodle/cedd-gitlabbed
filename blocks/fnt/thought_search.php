<?php

/** 
 * Fnt Block: Search 
 * 
 * @author      Kirill Astashov <kirill@androgogic.com> 
 * @version     02/09/2014 
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Search thoughts
 * Also provides access to edit and delete functions if user has sufficient permissions
 *  
 **/

//params
$sort   = optional_param('sort', 'date_created', PARAM_RAW);
$dir    = optional_param('dir', 'ASC', PARAM_ALPHA);
$page   = optional_param('page', 0, PARAM_INT);
$perpage= optional_param('perpage', 20, PARAM_INT); 
$search = optional_param('search', '', PARAM_TEXT);
$tab    = optional_param('tab', 'thought_search', PARAM_FILE);
// prepare url for paging bar
$PAGE->set_url($PAGE->url, compact('sort', 'dir', 'page','perpage','search','tab')); 
// prepare columns for results table
$columns = array(
    "datetimethought",
    "promptingevent",
    "unhelpthought",
    "emotions",
    "helpthoughts",
    "howifeel",
    "userid",
    );

$fromdate = 0;
$todate = 0;
if (!empty($_REQUEST['enabledatefilter']) && !empty($_REQUEST['fromdate']) && !empty($_REQUEST['todate'])) {
    $calendartype = \core_calendar\type_factory::get_calendar_instance();
    $gregoriandate = $calendartype->convert_to_gregorian($_REQUEST['fromdate']['year'], $_REQUEST['fromdate']['month'], $_REQUEST['fromdate']['day']);
    $fromtimestamp = make_timestamp($gregoriandate['year'],
                                              $gregoriandate['month'],
                                              $gregoriandate['day'],
                                              0, 0, 0,
                                              99,
                                              true);
    $gregoriandate = $calendartype->convert_to_gregorian($_REQUEST['todate']['year'], $_REQUEST['todate']['month'], $_REQUEST['todate']['day']);
    $totimestamp = make_timestamp($gregoriandate['year'],
                                              $gregoriandate['month'],
                                              $gregoriandate['day'],
                                              0, 0, 0,
                                              99,
                                              true);
    // include the 'to' day as well
    $totimestamp += 24 * 3600;
}


// check sort param
if (!in_array($sort, $columns)) {
    $sort = 'date_created';
}
foreach ($columns as $column) {
    $string[$column] = get_string("$column",'block_fnt');
    if ($sort != $column) {
        $columnicon = '';
        $columndir = 'ASC';
    } else {
        $columndir = $dir == 'ASC' ? 'DESC':'ASC';
        $columnicon = $dir == 'ASC' ? 'down':'up';
    }
    if($column == 'datetimethought'){
        $$column = "<a href='$PAGE->url&amp;dir=$columndir&amp;sort=$column'>$string[$column]</a>";
    } else {
        $$column = $string[$column];
    }
}
//figure out the and clause from what has been submitted
$search = addslashes($search);
$and = '';
if ($search != '') {
    $and .= " and concat(
    u.firstname,
    u.lastname,
    u.username
    ) like '%$search%'";
}
if (!has_capability('block/fnt:viewotherrecord', $context)) {
    $and .= "AND userid = {$USER->id}";
}
if (!empty($fromtimestamp)) {
    $and .= "
        AND datetimethought >= $fromtimestamp
        AND datetimethought <= $totimestamp
    ";
}
$q = "select DISTINCT a.* , CONCAT(u.firstname,' ', u.lastname, ' (' , u.username , ')' ) as userid
    from {block_fnt_thought} a
    LEFT JOIN {user} u on a.userid = u.id
    where 1 = 1
    $and
    order by $sort $dir";

//get a page worth of records
$results = $DB->get_records_sql($q,array(),$page*$perpage, $perpage);

//also get the total number we have of these
$q = "SELECT COUNT(DISTINCT a.id)
    from {block_fnt_thought} a
    LEFT JOIN {user} u on a.userid = u.id
     where 1 = 1 $and";
$result_count = $DB->get_field_sql($q);

if ($download == '') {
    require_once('thought_search_form.php');
    $mform = new thought_search_form(null, array('sort' => $sort,'dir'=>$dir,'perpage'=>$perpage,'search'=>$search,'tab'=>$currenttab ));
    $mform->display();
    echo '<table width="100%"><tr><td width="50%">';
    echo $result_count . ' ' . get_string('thought_plural','block_fnt') . " found" . '<br>';
    echo '</td><td style="text-align:right;">';
    if (has_capability('block/fnt:addrecord', $context)) {
        echo "<a href='index.php?tab=thought_new&courseid={$courseid}'>" . get_string('thought_new','block_fnt') . "</a>";
    }
    echo '</td></tr></table>';
}

//RESULTS
if (!$results) {
    if($download == ''){
        echo $OUTPUT->heading(get_string('noresults','block_fnt',$search));
    }
} else {
    $table = new html_table();
    $table->head = array (
        $datetimethought,
        $promptingevent,
        $unhelpthought,
        $emotions,
        $helpthoughts,
        $howifeel);
    if (has_capability('block/fnt:viewotherrecord', $context)) {
        $table->head[] = $userid;
    }
    $table->head[] = 'Action';
    $table->align = array ("left","left","left","left","left","left","left","left","left","left","left","left","left","left",);
    $table->width = "95%";
    $table->size = array("7%","7%","7%","7%","7%","7%","7%","7%","7%","7%","7%","7%","7%","7%",);
    foreach ($results as $result) {
        $view_link = '';//enable if wanted: "<a href='index.php?tab=thought_view&id=$result->id'>View</a> ";
        $edit_link = "";
        $delete_link = "<a href='index.php?tab=thought_delete&id=$result->id' onclick='return confirm(\"Are you sure you want to delete this item?\")'>Delete</a> ";
        $result->datetimethought = userdate($result->datetimethought);
        $emotionarr = explode("\n", trim($result->emotions));
        $emotionarr2 = array();
        foreach ($emotionarr as &$str) {
            $str = trim($str);
            $arr = explode('=', $str);
            $key = $arr[0];
            $emotionarr2[$key] = intval($arr[1]);
        }
        $ratingarr = explode("\n", trim($result->rating));
        $ratingarr2 = array();
        foreach ($ratingarr as &$str) {
            $str = trim($str);
            $arr = explode('=', $str);
            $key = $arr[0];
            $ratingarr2[$key] = intval($arr[1]);
        }
        $emarr = array();
        foreach ($emotionarr2 as $key => $val) {
            if ($val) {
                $emarr[] = "$key ({$ratingarr2[$key]})";
            }
        }
        $em = implode(', ', $emarr);
        $arr = array (
            "$result->datetimethought",
            "$result->promptingevent",
            "$result->unhelpthought",
            $em,
            "$result->helpthoughts",
            "$result->howifeel");
        if (has_capability('block/fnt:viewotherrecord', $context)) {
            $arr[] = $result->userid;
        }
        $arr[] = $view_link . $edit_link . $delete_link;
        $table->data[] = $arr;
    }
}
if (!empty($table)) {
    if ($download != '') {
        //export the table to whatever they asked for
        block_fnt_export_data($download,$table,$tab);
    } else {
        echo html_writer::table($table);
        $pagingbar = new paging_bar($result_count, $page, $perpage,$PAGE->url);
        $pagingbar->pagevar = 'page';
        echo $OUTPUT->render($pagingbar);
        echo 'Download: ';
        $download_report_url = $PAGE->url . "&download=csv&courseid={$courseid}";
        echo html_writer::link($download_report_url, 'csv');
        echo "&nbsp;";
        $download_report_url = $PAGE->url . "&download=excel&courseid={$courseid}";
        echo html_writer::link($download_report_url, 'excel');
    }
}

