<?php

/** 
 * Fnt Block: Create object 
 * 
 * @author      Kirill Astashov <kirill@androgogic.com> 
 * @version     08/09/2014 
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Create new block_fnt_food
 *  
 **/

global $OUTPUT;
require_capability('block/fnt:addrecord', $context);
require_once('food_edit_form.php');
$reporturl = new moodle_url('/blocks/fnt/index.php', array('tab' => 'food_search', 'courseid' => $courseid));
$mform = new block_fnt_food_edit_form();
if ($mform->is_cancelled()) {
    redirect($reporturl);
    exit;
}
if ($data = $mform->get_data()) {
    $data->created_by = $USER->id;
    $data->date_created = time();
    $coursecontext = context_course::instance($courseid, MUST_EXIST);
    if (empty($data->userid) || !has_capability('block/fnt:editotherrecord', $coursecontext)) {
        $data->userid = $USER->id;
    }
    
    // get latest diary entry for today
    $sql = "SELECT *
            FROM {block_fnt_food}
            WHERE userid = :userid
                AND courseid = :courseid
                AND dateconsumed = :dateconsumed
            ORDER BY dateconsumed DESC, timeate DESC LIMIT 1";
    $params = array(
        'userid' => $data->userid,
        'courseid' => $data->courseid,
        'dateconsumed' => $data->dateconsumed,
    );
    $rowprevioustoday = $DB->get_record_sql($sql, $params);
    if ($rowprevioustoday) {
        $ptconsumed = $rowprevioustoday->dateconsumed + block_fnt_sqltimetoseconds($rowprevioustoday->timeate);
    }

    echo $OUTPUT->notification(get_string('foodrecordadded','block_fnt'), 'notifysuccess');
    // echo $OUTPUT->action_link($PAGE->url, get_string('addanotherfoodrecord','block_fnt'));

    // CHECKS
    $thisconsumed = $data->dateconsumed + block_fnt_sqltimetoseconds($data->timeate);
    $reporturl = new moodle_url('/blocks/fnt/index.php', array('tab' => 'food_search', 'courseid' => $courseid));
    $notification = '';
    // echo $OUTPUT->continue_button($return);

    // 1. On submit, if a new entry is made and, for the first time for the day, the time selected
    // is between 160-220 minutes after the last entry, display the following message on submit.
    $afterlast160220 = false;
    if ($rowprevioustoday
            && $rowprevioustoday->dateconsumed == $data->dateconsumed
            && ($thisconsumed > $ptconsumed)
            && ($thisconsumed - $ptconsumed) >= (160 * 60)
            && ($thisconsumed - $ptconsumed) <= (220 * 60)
            ) {
        $notification = get_string('check1', 'block_fnt');
        $buttontext = get_string('understood', 'block_fnt');
        $afterlast160220 = true;
    }

    // 2. On submit, if a new entry is made and, for the second or more times for the day, the time selected is between
    // 160-220 minutes after the last entry, AND a case of a period of greater than 220 minutes, display the following
    // message on submit.
    if ($afterlast160220) {
        // check other records
        $sql = "SELECT *
                FROM {block_fnt_food}
                WHERE userid = :userid
                    AND courseid = :courseid
                    AND dateconsumed = :dateconsumed
                ORDER BY dateconsumed ASC, timeate ASC";
        $params = array(
            'userid' => $data->userid,
            'courseid' => $data->courseid,
            'dateconsumed' => $data->dateconsumed,
        );
        $rowstoday = $DB->get_records_sql($sql, $params);
        $oldconsumed = 0;
        $times160220 = 0;
        foreach ($rowstoday as $rowtoday ) {
            $newconsumed = $rowtoday->dateconsumed + block_fnt_sqltimetoseconds($rowtoday->timeate);
            if ($oldconsumed
                    && ($newconsumed - $oldconsumed) >= (160 * 60)
                    && ($newconsumed - $oldconsumed) <= (220 * 60)
                    ) {
                $times160220++;
            }
            $oldconsumed = $newconsumed;
        }
        // consider the current record is included as well, not counted cos not in the DB yet
        if ($times160220 >= 1) {
            $notification = get_string('check2', 'block_fnt');
        }
    }

    // 3. On submit, when entering first record for a new day
    // and for the previous day, there were <=1 records, display message:
    // 4. On submit, when entering first record for a new day and for the previous day, there were 2-4 records,
    // display message:
    // 5. On submit, when entering first record for a new day and for the previous day, there were 4-6 records,
    // display message:
    if (!$rowprevioustoday) {
        $yesterday = $data->dateconsumed - (24 * 3600);
        $sql = "SELECT *
                FROM {block_fnt_food}
                WHERE userid = :userid
                    AND courseid = :courseid
                    AND dateconsumed = :dateconsumed
                ORDER BY dateconsumed ASC, timeate ASC";
        $params = array(
            'userid' => $data->userid,
            'courseid' => $data->courseid,
            'dateconsumed' => $yesterday,
        );
        $rowsyesterday = $DB->get_records_sql($sql, $params);
        if (!$rowsyesterday || count($rowsyesterday) == 1) {
            $notification = get_string('check3', 'block_fnt');
        }
        if ($rowsyesterday && count($rowsyesterday) >= 2 && count($rowsyesterday) < 4 ) {
            $notification = get_string('check4', 'block_fnt');
        }
        if ($rowsyesterday && count($rowsyesterday) >= 4 && count($rowsyesterday) <= 6 ) {
            $notification = get_string('check5', 'block_fnt');
        }
    }
    
    // 6. On submit, when entering first record for a new day, if the time between ‘Time I had something to eat’
    // is >90 minutes after the from their stated wake up, time, display message:
    if (!$rowprevioustoday) {
        $consumedseconds = block_fnt_sqltimetoseconds($data->timeate);
        $wokeupseconds =  block_fnt_sqltimetoseconds($data->timewokeup);
        if (($consumedseconds - $wokeupseconds) > (90 * 60)) {
            $notification = get_string('check6', 'block_fnt');
        }
    }

    // 7. On submit, if has answer to question ‘Did you have an urge to binge’ = ‘No’, display message:
    if (!$data->urgetobinge) {
        $notification = get_string('check7', 'block_fnt');
    }

    // 8. On submit, answer to question ‘Did you have an urge to binge’ = ‘Yes’ and answer to question
    // ‘Did you consider it a binge?” = ‘No’, display message:
    if ($data->urgetobinge && !$data->didyoubinge) {
        $notification = get_string('check8', 'block_fnt');
    }

    // 9. On submit, if answer to question ‘Did you have an urge to binge’ = ‘Yes’
    // and answer to question ‘Did you binge?” = ‘Yes’, display message:
    if ($data->urgetobinge && $data->didyoubinge) {
        $notification = get_string('check9', 'block_fnt');
    }

    // 10. On submit, if answer to question ‘Did you have an urge to binge’ = ‘Yes’ and answer
    // to question ‘Did you binge?” = ‘Yes’ and either ‘Did you purge?’ = ‘Yes’
    // and/or ‘Did you take laxatives?’ = ‘Yes’, display message:
    if ($data->urgetobinge && $data->didyoubinge && ($data->didyoupurge || $data->didtakelaxatives)) {
        $notification = get_string('check10', 'block_fnt');
    }
    
    // 11. On submit, if ‘Urge to binge?’ =’No’ and all answers to the questions under
    // the ‘Compensatory behaviour’ section = ‘No’, display message:
    if (!$data->urgetobinge && !$data->didyoupurge && !$data->didyouexcercise && !$data->didtakelaxatives && !$data->didyouelse) {
        $notification = get_string('check11', 'block_fnt');
    }

    // 12. On submit, if answer to question ‘Did you have an urge to binge’ = ‘Yes’ and
    // ‘Did you consider it a binge?’ = ‘No’ and all answers to the questions under the ‘Compensatory behaviour’
    // section = ‘No’, display message:
    if ($data->urgetobinge && !$data->didyoubinge && !$data->didyoupurge && !$data->didyouexcercise && !$data->didtakelaxatives && !$data->didyouelse) {
        $notification = get_string('check12', 'block_fnt');
    }

    // 13. On submit, if answer to question ‘Did you have an urge to binge’ = ‘Yes’ and answer to question
    // ‘Did you consider it a binge? = ‘Yes’ all answers to the questions under the ‘Compensatory behaviour’
    // section = ‘No’, display message:
    if ($data->urgetobinge && $data->didyoubinge && !$data->didyoupurge && !$data->didyouexcercise && !$data->didtakelaxatives && !$data->didyouelse) {
        $notification = get_string('check13', 'block_fnt');
    }

    // insert record
    $newid = $DB->insert_record('block_fnt_food',$data);

    if ($notification) {
        echo $OUTPUT->notification($notification);
    }
    if (empty($buttontext)) {
        $buttontext = get_string('proceedfoodreport', 'block_fnt');
    }
    echo $OUTPUT->single_button($reporturl, $buttontext);

} else{
    echo $OUTPUT->heading(get_string('food_new', 'block_fnt'));
    $mform->display();
}

