<?php
/** 
 * Fnt Block: Tabs 
 * 
 * @author      Kirill Astashov <kirill@androgogic.com> 
 * @version     02/09/2014 
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 *  
 **/

// This file to be included so we can assume config.php has already been included.
if (empty($currenttab)) {
    error('You cannot call this script in that way');
}
$tabs = array();
$row = array();
if (block_fnt_checkenrolment($config->coursesfood)) {
    $row[] = new tabobject('food_search', $CFG->wwwroot.'/blocks/fnt/index.php?tab=food_search&courseid=' . $courseid, get_string('food_search','block_fnt'));
}
if (block_fnt_checkenrolment($config->coursesthought)) {
    $row[] = new tabobject('thought_search', $CFG->wwwroot.'/blocks/fnt/index.php?tab=thought_search&courseid=' . $courseid, get_string('thought_search','block_fnt'));
}
if (block_fnt_checkenrolment($config->coursesfear)) {
    $row[] = new tabobject('fear_search', $CFG->wwwroot.'/blocks/fnt/index.php?tab=fear_search&courseid=' . $courseid, get_string('fear_search','block_fnt'));
}
$tabs[] = $row;
// Print out the tabs and continue!
print_tabs($tabs, $currenttab);

// End of blocks/fnt/tabs.php