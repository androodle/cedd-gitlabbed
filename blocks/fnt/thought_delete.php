<?php

/** 
 * Fnt Block: Delete object 
 * 
 * @author      Kirill Astashov <kirill@androgogic.com> 
 * @version     02/09/2014 
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Delete one of the thoughts
 *  
 **/

$id = required_param('id', PARAM_INT);
$DB->delete_records('block_fnt_thought',array('id'=>$id));
echo $OUTPUT->notification(get_string('itemdeleted','block_fnt'), 'notifysuccess');

