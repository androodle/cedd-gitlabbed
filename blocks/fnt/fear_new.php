<?php

/** 
 * Fnt Block: Create object 
 * 
 * @author      Kirill Astashov <kirill@androgogic.com> 
 * @version     02/09/2014 
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Create new thought
 *  
 **/

global $OUTPUT;
require_capability('block/fnt:addrecord', $context);
require_once('fear_edit_form.php');
$reporturl = new moodle_url('/blocks/fnt/index.php', array('tab' => 'fear_search', 'courseid' => $courseid));
$mform = new fear_edit_form();
if ($mform->is_cancelled()) {
    redirect($reporturl);
    exit;
}
if ($data = $mform->get_data()){
    $data->created_by = $USER->id;
    $data->date_created = time();
    if (empty($data->userid) || !has_capability('block/fnt:editotherrecord', $context)) {
        $data->userid = $USER->id;
    }
    $newid = $DB->insert_record('block_fnt_fear',$data);
    echo $OUTPUT->notification(get_string('datasubmitted','block_fnt'), 'notifysuccess');
    echo $OUTPUT->single_button($reporturl, get_string('proceedtofearreport', 'block_fnt'));
    // echo $OUTPUT->action_link($PAGE->url, 'Create another item');
} else{
    echo $OUTPUT->heading(get_string('fear_new', 'block_fnt'));
    $mform->display();
}

