<?php

function block_fnt_export_data($download_format,$table,$filename){
    $stripped_table = block_fnt_strip_tags_from_table($table);
    $function = "block_fnt_export_data_".$download_format;
    $function($stripped_table,$filename);
}

function block_fnt_export_data_excel($table,$filename){
    global $CFG;
    require_once("$CFG->libdir/excellib.class.php");
    $filename = clean_filename($filename).'.xls';
    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);
    $worksheet = array();
    $worksheet[0] = $workbook->add_worksheet('');
    $col = 0;
    foreach ($table->head as $fieldname) {
        $worksheet[0]->write(0, $col, $fieldname);
        $col++;
    }
    $row = 1;
    foreach ($table->data as $row_data) {
        $col = 0;
        foreach ($row_data as $cell) {
            $worksheet[0]->write($row, $col, $cell);
            $col++;
        }
        $row++;
    }
    $workbook->close();
    die;
}

function block_fnt_export_data_csv($table,$filename){
    global $CFG;
    require_once($CFG->libdir . '/csvlib.class.php');
    $filename = clean_filename($filename).'.csv';
    $csvexport = new csv_export_writer();
    $csvexport->set_filename($filename);
    $csvexport->add_data($table->head);
    foreach ($table->data as $row_data) {
        $csvexport->add_data($row_data);
    }
    $csvexport->download_file();
    die;
}

function block_fnt_strip_tags_from_table($table) {
    //strip any html out of the table head and rows
    $stripped_table = new stdClass();
    foreach($table->head as $table_head_cell){
        $stripped_table->head[] = strip_tags($table_head_cell);
    }
    foreach ($table->data as $row_data) {
        $stripped_row_data = array();
        foreach($row_data as $cell){
            $stripped_row_data[] = strip_tags($cell);
        }
        $stripped_table->data[] = $stripped_row_data;
    }
    return $stripped_table;
}

// converts time in string format "7:15" or "01:23:45" to seconds since midnight,
// return int
function block_fnt_sqltimetoseconds($str) {
    $arr = explode(':', $str);
    if (!isset($arr[2])) {
        $arr[2] = 0;
    }
    return 3600 * $arr[0] + 60 * $arr[1] + $arr[2];
}

// creates a DB field value from inputs (emotions and ratings)
function block_fnt_populatefromgroup($data, $suffix) {
    global $emotionsarr;
    $out = '';
    foreach ($emotionsarr as $val) {
        $vlower = strtolower($val);
        $inputname = $vlower . '_' . $suffix;
        $outval = (empty($data->$inputname)) ? '0' : $data->$inputname;
        $out .= $val . '=' . $outval . "\n";
    }
    return $out;
}

/*
 * checks if user is enrolled in ALL of the courses from a comma-delimited list
 * site admin is allowed to do/see anything, so always true for admins
 *
 * param $str - comma delimited list of course IDs
 * return boolean
 */
function block_fnt_checkenrolment($str) {
    global $DB, $USER;

    if (is_siteadmin()) {
        return true;
    }

    // cache enrolments
    static $enrolments = array();
    static $notenrolled = array();

    $ret = true;

    $courses = explode(',', $str);
    foreach ($courses as &$course) {
        $course = intval(trim($course));
        if (!empty($course) && !in_array($course, $notenrolled)) {
            if (!$DB->record_exists('course', array('id' => $course))) {
                $notenrolled[] = $course;
                $ret = false;
                break;
            }
            if ($coursecontext = context_course::instance($course)) {
                if (in_array($course, $enrolments) || is_enrolled($coursecontext, $USER, '', true)) {
                    if (!in_array($course, $enrolments)) {
                        $enrolments[] = $course;
                    }
                    continue;
                } else {
                    $notenrolled[] = $course;
                    $ret = false;
                    break;
                }
            }
        }
    }
    return $ret;
}

// checks user's enrolment and throws error in case the requirement is not met
function block_fnt_require_enrolment($str, $courseid) {
    global $CFG;

    if (!block_fnt_checkenrolment($str)) {
        notice(get_string('accessdenied', 'block_fnt'), $CFG->wwwroot . '/course/view.php?id=' . $courseid);
    }
}