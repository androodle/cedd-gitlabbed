<?php

/** 
 * Fnt Block: Edit form 
 * 
 * @author      Kirill Astashov <kirill@androgogic.com> 
 * @version     02/09/2014 
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Provides edit form for the object.
 * This is used by both new and edit pages
 *  
 **/

if (!defined('MOODLE_INTERNAL')) {
    die ('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');
class fear_edit_form extends moodleform {
    protected $thought;
    function definition() {
        global $USER, $courseid, $DB, $PAGE, $fearrating;
        $mform =& $this->_form;
        $coursecontext = context_course::instance($courseid);
        $id = optional_param('id', 0, PARAM_INT);
        if ($id) {
            $q = "select DISTINCT a.* , CONCAT(u.firstname,' ',u.lastname, ' (', u.username, ')') as uname
            from {block_fnt_fear} a
            LEFT JOIN {user} u on a.userid = u.id
            where a.id = $id";
            $thought = $DB->get_record_sql($q);
        } else {
            $thought = $this->_customdata['$thought']; // this contains the data of this form
        }
        $tab = 'fear_new'; // from whence we were called
        if (!empty($thought->id)) {
            $tab = 'fear_edit';
        }
        $mform->addElement('html','<div>');

        $editoroptions = array('rows' => 5, 'cols' => 80);
        $config = get_config('block_fnt');

        // Forbidden Food
        $config->forbiddenfoodlist = trim($config->forbiddenfoodlist);
        $foodlistarr = explode("\n", $config->forbiddenfoodlist);
        $foodlist = array();
        foreach ($foodlistarr as &$food) {
            $food = trim($food);
            if (!empty($food)) {
                $foodlist[$food] = $food;
            }
        }
        $mform->addElement('select', 'forbiddenfood', get_string('forbiddenfood','block_fnt'), $foodlist);
        $mform->setType('forbiddenfood', PARAM_TEXT);

        // Fear rating
        $mform->addElement('select', 'rating', get_string('fearrating', 'block_fnt'), $fearrating);
        $mform->setType('rating', PARAM_INT);

        // Triggered a binge
        $mform->addElement('selectyesno', 'triggeredbinge', get_string('triggeredbinge','block_fnt'));

        // No consequence
        $mform->addElement('selectyesno', 'noconsequences', get_string('noconsequences','block_fnt'));

        //userid
        if (has_capability('block/fnt:editotherrecord', $coursecontext)) {
            $users = get_enrolled_users($coursecontext, '', 0, 'u.*', 'u.firstname ASC, u.lastname ASC, u.username ASC');
            $usersarr = array();
            foreach ($users as $user) {
                $usersarr[$user->id] = "$user->firstname $user->lastname ({$user->username})";
            }
            $select = $mform->addElement('searchableselector', 'userid', get_string('selectuser', 'block_fnt'), $usersarr);
            $select->setMultiple(false);
        }

        //set values if we are in edit mode
        if (!empty($thought->id) && $id) {
            $mform->setConstant('datetimethought', strtotime($thought->datetimethought));
            $promptingevent['text'] = $thought->promptingevent;
            $promptingevent['format'] = 1;
            $mform->setDefault('promptingevent', $promptingevent);
            $unhelpthought['text'] = $thought->unhelpthought;
            $unhelpthought['format'] = 1;
            $mform->setDefault('unhelpthought', $unhelpthought);
            $helpthoughts['text'] = $thought->helpthoughts;
            $helpthoughts['format'] = 1;
            $mform->setDefault('helpthoughts', $helpthoughts);
            $howifeel['text'] = $thought->howifeel;
            $howifeel['format'] = 1;
            $mform->setDefault('howifeel', $howifeel);
            $mform->setConstant('userid', $thought->userid);
        }
        //hiddens
        $mform->addElement('hidden','tab',$tab);
        $mform->setType('tab', PARAM_CLEANHTML);
        $mform->addElement('hidden', 'courseid', $courseid);
        $mform->setType('courseid', PARAM_INT);
        if ($id) {
            $mform->addElement('hidden','id',$_REQUEST['id']);
            $mform->setType('id', PARAM_INT);
        }
        $this->add_action_buttons(true);
        $mform->addElement('html','</div>');
    }
}
