<?php 
// enroll student to course (roleid = 5 is student role)
function enroll_to_course($courseid, $userid, $roleid=5, $extendbase=3, $extendperiod=0)  {
    global $DB;

    $instance = $DB->get_record('enrol', array('courseid' => $courseid, 'enrol' => 'manual'), '*', MUST_EXIST);
    $course = $DB->get_record('course', array('id' => $instance->courseid), '*', MUST_EXIST);
    $today = time();
    $today = make_timestamp(date('Y', $today), date('m', $today), date('d', $today), 0, 0, 0);

    if (!$enrol_manual = enrol_get_plugin('manual')) { throw new coding_exception('Can not instantiate enrol_manual'); }
    switch($extendbase) {
        case 2:
            $timestart = $course->startdate;
            break;
        case 3:
        default:
            $timestart = $today;
            break;
    }  
    if ($extendperiod <= 0) { $timeend = 0; }   // extendperiod are seconds
    else { $timeend = $timestart + $extendperiod; }
    $enrolled = $enrol_manual->enrol_user($instance, $userid, $roleid, $timestart, $timeend);
    //add_to_log($course->id, 'course', 'enrol', '../enrol/users.php?id='.$course->id, $course->id);

    return $enrolled;
}





function local_cron_function() {
    global $DB;
    $sql_category = "SELECT id from {course_categories} where name='cedd' ";
    $cedd_category_id = $DB->get_record_sql($sql_category);
    $courses = get_courses($cedd_category_id->id, 'idnumber');
    //print_object($courses);
    $students_list = null;
    
    //Now we loop through the courses and get the course where idnumber=1 (sesssion One) and then get list of all the users enrolled in session one
    
    foreach ($courses as $course) {
       
        if ($course->idnumber == '1') {
            //this is Session One
            $context = context_course::instance($course->id);
            $students_list = get_role_users(5 , $context);
            
        }
    }
    
    //Now we have list of student enrolled in session one. Check for each student, if they have completed the session/course, enroll them to next session in sequence and so on
    
    foreach ($students_list as $cedd_student) {
        
        foreach ($courses as $student_course) {

            $context = context_course::instance($student_course->id);
            if (is_enrolled($context, $cedd_student->id, '', true)) {
                $cinfo = new completion_info($student_course);
                $iscomplete = $cinfo->is_course_complete($cedd_student->id);
                               
                if ($iscomplete) {
                    //echo "<br> Course is complete and idnumber is=". $student_course->idnumber;
                    switch ($student_course->idnumber) {
                        
                        case '1':
                            //echo "Session One Case";
                            $sql = "Select id from {block_fnt_food} Where userid = ".$cedd_student->id;
                            if ($DB->record_exists_sql($sql)) {
                                //User has atleast one entry in food diary
                                                              
                                // Now check completion date/time
                                $sql = "Select * from {course_completions} Where userid = ". $cedd_student->id . " AND course = " . $student_course->id;
                                $completion_record = $DB->get_record_sql($sql);
                               
                                //echo "Time Completed =" . date('m-d-Y',$completion_record->timecompleted);
                                if ( strtotime("now") >= strtotime( "+1 day" , $completion_record->timecompleted) ) {
                             
                                $sql_to_enroll = "Select id, category, fullname, shortname, idnumber from {course} Where idnumber = ". ($student_course->idnumber + 1);
                                $enrol_to  = $DB->get_record_sql($sql_to_enroll);
                                if ($enrol_to) {
                                $context = context_course::instance($enrol_to->id);
                                   
                                if (! is_enrolled($context, $cedd_student->id, '', true)) {
                                       // if not already enrolled then enrol to next session (Session Two)
                                       enroll_to_course($enrol_to->id, $cedd_student->id);
                                       //echo "student enrolled to next session (Session Two)";
                                     }
                                         
                                }
                                }
                                
                            }
                           
                           break;
                        
                           
                           
                        case '2':
                            
                            //echo "Session Two Case";
                            $sql = "Select id from {block_fnt_thought} Where userid = ".$cedd_student->id;
                            if ($DB->record_exists_sql($sql)) {
                                //User has atleast one entry in thoughts diary entry
                                                              
                                // Now check if user have done another Food diary entry after enrolling to session 2
                                $sql = "SELECT id from {block_fnt_food} WHERE userid =".$cedd_student->id." AND date_created > ( SELECT ue.timecreated FROM {user_enrolments} ue JOIN {enrol} e  ON ue.enrolid = e.id WHERE ue.userid = ".$cedd_student->id." AND e.courseid = ".$student_course->id.") ";
                                if ($DB->record_exists_sql($sql)){
                                    //echo "New condition met.";
                               
                                // Now check completion date/time
                                $sql = "Select * from {course_completions} Where userid = ". $cedd_student->id . " AND course = " . $student_course->id;
                                $completion_record = $DB->get_record_sql($sql);
                               
                                //echo "Time Completed =" . date('m-d-Y',$completion_record->timecompleted);
                                if ( strtotime("now") >= strtotime( "+1 day" , $completion_record->timecompleted) ) {
                             
                                $sql_to_enroll = "Select id, category, fullname, shortname, idnumber from {course} Where idnumber = ". ($student_course->idnumber + 1);
                                $enrol_to  = $DB->get_record_sql($sql_to_enroll);
                                
                                if ($enrol_to) {
                                $context = context_course::instance($enrol_to->id);
                                         
                                
                                if (! is_enrolled($context, $cedd_student->id, '', true)) {
                                       // if not already enrolled then enrol to next session (Session Two)
                                       enroll_to_course($enrol_to->id, $cedd_student->id);
                                       //echo "student enrolled to next session (Session Three)";
                                     }
                                         
                                }
                                
                                }
                                
                            }
                            }
                            break;
                        
                            
                            
                        case '3':
                            
                            //echo "Session Three Case";
                            $sql = "Select id from {block_fnt_fear} Where userid = ".$cedd_student->id;
                            if ($DB->record_exists_sql($sql)) {
                                //User has atleast one entry in fear food diary
                                
                                
                                // Now check if user have added one more entry to food diary after being enrolled to Session 3
                                $sql = "SELECT id from {block_fnt_food} WHERE userid =".$cedd_student->id." AND date_created > ( SELECT ue.timecreated FROM {user_enrolments} ue JOIN {enrol} e  ON ue.enrolid = e.id WHERE ue.userid = ".$cedd_student->id." AND e.courseid = ".$student_course->id.") ";
                                if ($DB->record_exists_sql($sql)){
                                    //echo "New condition met in session 3.";
                                
                                    
                                // Now check if user have added one more entry to thoughts diary after being enrolled to Session 3
                                $sql = "SELECT id from {block_fnt_thought} WHERE userid =".$cedd_student->id." AND date_created > ( SELECT ue.timecreated FROM {user_enrolments} ue JOIN {enrol} e  ON ue.enrolid = e.id WHERE ue.userid = ".$cedd_student->id." AND e.courseid = ".$student_course->id.") ";
                                if ($DB->record_exists_sql($sql)){
                                    //echo "New condition met in session 3.";
                                
                             
                                // Now check completion date/time
                                $sql = "Select * from {course_completions} Where userid = ". $cedd_student->id . " AND course = " . $student_course->id;
                                $completion_record = $DB->get_record_sql($sql);
                               
                                
                                //echo "Time Completed =" . date('m-d-Y',$completion_record->timecompleted);
                                if ( strtotime("now") >= strtotime( "+1 day" , $completion_record->timecompleted) ) {
                             
                                $sql_to_enroll = "Select id, category, fullname, shortname, idnumber from {course} Where idnumber = ". ($student_course->idnumber + 1);
                                $enrol_to  = $DB->get_record_sql($sql_to_enroll);
                                //var_dump($enrol_to);
                                if ($enrol_to) {
                                $context = context_course::instance($enrol_to->id);
                                         
                                
                                if (! is_enrolled($context, $cedd_student->id, '', true)) {
                                       // if not already enrolled then enrol to next session (Session Two)
                                       enroll_to_course($enrol_to->id, $cedd_student->id);
                                       //echo "Student enrolled to next session (Session Four)";
                                     }
                                         
                                }
                                }
                                }
                                }
                            }
                            break;
                        
                        
                        
                        default:
                            
                            // Now check completion date/time
                                //echo "In the default section.";
                                
                                // Now check if user have added one more entry to food diary after being enrolled in current session
                                $sql = "SELECT id from {block_fnt_food} WHERE userid =".$cedd_student->id." AND date_created > ( SELECT ue.timecreated FROM {user_enrolments} ue JOIN {enrol} e  ON ue.enrolid = e.id WHERE ue.userid = ".$cedd_student->id." AND e.courseid = ".$student_course->id.") ";
                                if ($DB->record_exists_sql($sql)){
                                    //echo "New condition met in session". $student_course->idnumber;
                                    
                                
                                // Now check if user have added one more entry to thought diary after being enrolled in current session
                                $sql = "SELECT id from {block_fnt_thought} WHERE userid =".$cedd_student->id." AND date_created > ( SELECT ue.timecreated FROM {user_enrolments} ue JOIN {enrol} e  ON ue.enrolid = e.id WHERE ue.userid = ".$cedd_student->id." AND e.courseid = ".$student_course->id.") ";
                                if ($DB->record_exists_sql($sql)){
                                    //echo "New condition met in session". $student_course->idnumber;
                                    
                                    
                                // Now check if user have added one more entry to fear food diary after being enrolled in current session
                                $sql = "SELECT id from {block_fnt_fear} WHERE userid =".$cedd_student->id." AND date_created > ( SELECT ue.timecreated FROM {user_enrolments} ue JOIN {enrol} e  ON ue.enrolid = e.id WHERE ue.userid = ".$cedd_student->id." AND e.courseid = ".$student_course->id.") ";
                                if ($DB->record_exists_sql($sql)){
                                //echo "New condition met in session". $student_course->idnumber;
                                    
                                
                                $sql = "Select * from {course_completions} Where userid = ". $cedd_student->id . " AND course = " . $student_course->id;
                                $completion_record = $DB->get_record_sql($sql);
                               
                                //echo "Time Completed =" . date('m-d-Y',$completion_record->timecompleted);
                                if ( strtotime("now") >= strtotime( "+1 day" , $completion_record->timecompleted) ) {
                             
                                $sql_to_enroll = "Select id, category, fullname, shortname, idnumber from {course} Where idnumber = ". ($student_course->idnumber + 1);
                                $enrol_to  = $DB->get_record_sql($sql_to_enroll);
                                
                                if ($enrol_to) {
                                $context = context_course::instance($enrol_to->id);
                                         
                                
                                if (! is_enrolled($context, $cedd_student->id, '', true)) {
                                       // if not already enrolled then enrol to next session (Session Two)
                                       enroll_to_course($enrol_to->id, $cedd_student->id);
                                       //echo "student enrolled to next session". $student_course->idnumber;
                                     }
                                         
                                }
                                }
                                }
                                }
                                }
                            break;
                            
                            
                    }
                }
            }
            
        }
    }
}