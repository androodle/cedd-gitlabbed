<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * 
 *
 * @package    block_cedd_session_mgt
 * @copyright  Sohail Aslam <sohail.aslam@mq.edu.au>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */



defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/blocks/cedd_session_mgt/locallib.php');

class block_cedd_session_mgt extends block_base {

    function init() {
        $this->title = get_string('pluginname', 'block_cedd_session_mgt');
    }

    public function hide_header() {
        return true;
    }


    function get_content() {
        global $CFG, $OUTPUT, $DB, $USER ;

        if ($this->content !== null) {
            return $this->content;
        }

        if (empty($this->instance)) {
            $this->content = '';
            return $this->content;
        }

        $this->content = new stdClass();
        $this->content->text = '';
        $this->content->footer = '';
        //local_cron_function();
        
        $sql  = "SELECT id, category, fullname, shortname, idnumber from {course} where category = ( SELECT id from {course_categories} where name='cedd') order by idnumber";
        $rs = $DB->get_recordset_sql($sql);
        
        if ($rs->valid()) {       
                        $this->content->text .= html_writer::start_tag('ul', array('class' => 'session_list'));
                        foreach ($rs as $row_main) {
                               
                                /* creating list */
                            
                                //var_dump($row_main);
                                $course_seq_number = $row_main->idnumber;
                                //print_object(get_array_of_activities($row_main->id));
                                //print_object(get_fast_modinfo($row_main->id));
                                if ($course_seq_number == '1') {
                                    $courseurl = new moodle_url($CFG->wwwroot. '/course/view.php', array('id' => $row_main->id));
                                    $this->content->text .= html_writer::start_tag('li', array('class' => 'session_unlocked'));
                                    $this->content->text .= html_writer::link($courseurl, $row_main->fullname);
                                    $this->content->text .= html_writer::end_tag('li');
                                    } else {
                                     
                                            $context = context_course::instance($row_main->id);
                                     
                                            if (is_enrolled($context, $USER->id, '', true)) {
                                                $courseurl = new moodle_url($CFG->wwwroot. '/course/view.php', array('id' => $row_main->id));
                                                $this->content->text .= html_writer::start_tag('li', array('class' => 'session_unlocked'));
                                                $this->content->text .= html_writer::link($courseurl, $row_main->fullname);
                                                $this->content->text .= html_writer::end_tag('li');     
                                                } else {
                                                $courseurl = new moodle_url($CFG->wwwroot. '/course/view.php', array('id' => $row_main->id));
                                                $this->content->text .= html_writer::start_tag('li', array('class' => 'session_locked'));
                                                $this->content->text .= html_writer::tag('span', $row_main->fullname); 
                                                $this->content->text .= html_writer::end_tag('li'); 
                                                }
                                     
                                           
                                            }
                                     
                           }
                        
                         $this->content->text .= html_writer::end_tag('ul');          
                                                
                           
                      
            }
                        
           
        $rs->close();
     
        return $this->content;
    }

    // my moodle can only have SITEID and it's redundant here, so take it away
    public function applicable_formats() {
        return array('all' => true);
    }
/*
    public function instance_allow_multiple() {
          return true;
    }

    function has_config() {
        return true;
        
    }
    
    function instance_allow_config() {
    return true;
    }

    
    public function instance_config_save($data) {
        if(get_config('cedd_session_mgt', 'foo') == '1') {
        $data->text = strip_tags($data->text);
    }
 
    // And now forward to the default implementation defined in the parent class
    return parent::instance_config_save($data);
    }

*/

    public function cron() {
         mtrace( "Session Management local cron function executing." );
         // do something
         local_cron_function();
         
         return true;
    }
    
    
    
    
}
