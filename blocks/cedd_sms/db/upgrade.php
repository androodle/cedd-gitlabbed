<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

// This file keeps track of upgrades to
// the escreen module
//
// Sometimes, changes between versions involve
// alterations to database structures and other
// major things that may break installations.
//
// The upgrade function in this file will attempt
// to perform all the necessary actions to upgrade
// your older installation to the current version.
//
// If there's something it cannot do itself, it
// will tell you what you need to do.
//
// The commands in here will all be database-neutral,
// using the methods of database_manager class
//
// Please do not forget to use upgrade_set_timeout()
// before any action that may take longer time to finish.

function xmldb_block_cedd_sms_upgrade($oldversion) {
    global $CFG, $DB;

    $dbman = $DB->get_manager();


    // Moodle v2.2.0 release upgrade line
    // Put any upgrade step following this

    // Moodle v2.3.0 release upgrade line
    // Put any upgrade step following this


    // Moodle v2.4.0 release upgrade line
    // Put any upgrade step following this


    // Moodle v2.5.0 release upgrade line.
    // Put any upgrade step following this.


    // Moodle v2.6.0 release upgrade line.
    // Put any upgrade step following this.

    // Moodle v2.7.0 release upgrade line.
    // Put any upgrade step following this.

	 if ($oldversion < 2014112001) {

        $table = new xmldb_table('block_cedd_sms');
		// Define field templateid to be added to block_cedd_sms.
		$template_field = new xmldb_field('templateid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null, 'id');
		// Define field courseid to be added to block_cedd_sms.
        $course_field = new xmldb_field('courseid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null, 'templateid');
		 
		// Conditionally launch add field templateid.
        if (!$dbman->field_exists($table, $template_field)) {
            $dbman->add_field($table, $template_field);
        }
		
        // Conditionally launch add field courseid.
        if (!$dbman->field_exists($table, $course_field)) {
            $dbman->add_field($table, $course_field);
        }
		

        // Cedd_sms savepoint reached.
        upgrade_block_savepoint(true, 2014112001, 'cedd_sms');
    }
	
	if ($oldversion < 2014112002) {

        // Define key templateid (foreign) to be added to block_cedd_sms.
        $table = new xmldb_table('block_cedd_sms');
        $key = new xmldb_key('templateid', XMLDB_KEY_FOREIGN, array('templateid'), 'block_cedd_sms_template', array('id'));

        // Launch add key templateid.
        $dbman->add_key($table, $key);
		
		// Define index courseid (not unique) to be added to block_cedd_sms.
		$index = new xmldb_index('courseid', XMLDB_INDEX_NOTUNIQUE, array('courseid'));

        // Conditionally launch add index courseid.
        if (!$dbman->index_exists($table, $index)) {
            $dbman->add_index($table, $index);
        }

        // Cedd_sms savepoint reached.
        upgrade_block_savepoint(true, 2014112002, 'cedd_sms');
    }
	
	 if ($oldversion < 2014112003) {

        // Define field ttype to be added to block_cedd_sms_template.
        $table = new xmldb_table('block_cedd_sms_template');
        $field = new xmldb_field('ttype', XMLDB_TYPE_INTEGER, '2', null, null, null, null, 'template');

        // Conditionally launch add field ttype.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Cedd_sms savepoint reached.
        upgrade_block_savepoint(true, 2014112003, 'cedd_sms');
    }
    return true;
}


