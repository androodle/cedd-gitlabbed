<?php
$tasks = array(                                                                                                                     
    array(                                                                                                                          
        'classname' => 'block_cedd_sms\task\cedd_send_morning_sms',                                                                            
        'blocking' => 0,                                                                                                            
        'minute' => '*/1',                                                                                                            
        'hour' => '*',                                                                                                              
        'day' => '*',                                                                                                               
        'dayofweek' => '*',                                                                                                         
        'month' => '*'                                                                                                              
    ),
	array(                                                                                                                          
        'classname' => 'block_cedd_sms\task\cedd_send_evening_sms',                                                                            
        'blocking' => 0,                                                                                                            
        'minute' => '*/1',                                                                                                            
        'hour' => '*',                                                                                                              
        'day' => '*',                                                                                                               
        'dayofweek' => '*',                                                                                                         
        'month' => '*'                                                                                                              
    )
);