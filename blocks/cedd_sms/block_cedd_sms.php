<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/* CEDD SMS Notifications Block
 * CEDD SMS Notifications is a one way SMS messaging block customised for the CEDD project.
 * It allows administrators to send daily SMS reminders to students based on course, at scheduled times
 * in the morning and evening. Based on the SMS Notifier Block by Azmat Ullah, Talha Noor.
 * @package blocks
 * @author: Johanna Follett for AccessMQ <johanna.follett@mq.edu.au>
 * @date: 28-Nov-2014
*/

defined('MOODLE_INTERNAL') || die();

class block_cedd_sms extends block_base {

    public function init() {
        $this->title = get_string('cedd_sms', 'block_cedd_sms');
    }
	
    public function get_content() {
	
        global $CFG, $USER, $COURSE;
		
        if ($this->content !== null) {
            return $this->content;
        }
		
		if (empty($this->instance)) {
            $this->content = '';
            return $this->content;
        }
		
        $this->content =  new stdClass;
        $this->content->text = html_writer::link(new moodle_url('/blocks/cedd_sms/view.php', array('viewpage' => '1')), get_string('sms_create', 'block_cedd_sms')).'<br>';   
		$this->content->text .= html_writer::link(new moodle_url('/blocks/cedd_sms/view.php'), get_string('sms_view', 'block_cedd_sms')).'<br>';
        return $this->content;
    }
	
    public function has_config() {
        return true;
    }
	
    public function applicable_formats() {
        return array('all' => true);
    }
	
    public function instance_allow_config() {
        return true;
    }
	
    public function specialization() {
        if (!empty($this->config->title)) {
            $this->title = $this->config->title;
        } 
    }

}   