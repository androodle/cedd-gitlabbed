<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/* CEDD SMS Notifications Block
 * CEDD SMS Notifications is a one way SMS messaging block customised for the CEDD project.
 * It allows administrators to send daily SMS reminders to students based on course, at scheduled times
 * in the morning and evening. Based on the SMS Notifier Block by Azmat Ullah, Talha Noor.
 * @package blocks
 * @author: Johanna Follett for AccessMQ <johanna.follett@mq.edu.au>
 * @date: 28-Nov-2014
*/

require_once("{$CFG->libdir}/formslib.php");
require_once("lib.php");

// Display SMS Template.
class template_form extends moodleform {
    public function definition() {
		global $DB;
		
        $mform =& $this->_form;
        $mform->addElement('header', 'sms_template_header', get_string('sms_template_header', 'block_cedd_sms'));
		
		//Add the Name text field.
        $mform->addElement('text', 'tname', 'Name:', array('size' => 44, 'maxlength' => 160));
        $mform->addRule('tname', 'Please Insert Template Name', 'required', 'client');
        $mform->setType('tname', PARAM_TEXT);
		
		//Add the Message Type select dropdown.
		$attributes = cedd_get_message_types();
        $mform->addElement('select', 'ttype', get_string('selecttype', 'block_cedd_sms'), $attributes);
		$mform->addRule('ttype', 'Please select a Message Type', 'required', null, 'server');
		$mform->setType('ttype', PARAM_INT);
		$mform->setDefault('ttype', 1); //Defaults to 'Morning'.
		
		//Add the Message textarea
        $mform->addElement('textarea', 'template', 'Message:', array('rows' => '6', 'cols' => '47', 'maxlength' => '160', 'id' => 'asd123'));
        $mform->addRule('template', 'Please Insert Template Message', 'required', 'client');
        $mform->setType('template', PARAM_TEXT);
		
		//Get the attributes needed to create the Select Course dropdown.
		if(isset($c_id)) {
            $attributes =  $DB->get_records_sql_menu('SELECT id , fullname FROM {course} where id = ?', array ($c_id), $limitfrom=0, $limitnum=0);
        }
        else {
			$category_id = cedd_get_session_category_id();
            $attributes =  $DB->get_records_sql_menu('SELECT id , fullname FROM {course} WHERE category = ?', array ( $category_id ), $limitfrom=0, $limitnum=0);
        }
		
		//Add the Select Course dropdown
        $select = $mform->addElement('select', 'courseid', get_string('selectcourse', 'block_cedd_sms'), $attributes);
		$select->setMultiple(true);
		$mform->addRule('courseid', 'Please select a Session', 'required', null, 'server');
		$mform->setType('courseid', PARAM_INT);
		
		$mform->setType('viewpage', PARAM_INT);
		$mform->setType('id', PARAM_INT);
		
        $mform->addElement('hidden', 'viewpage');
        $mform->addElement('hidden', 'id');
        $this->add_action_buttons();
    }
	
    public function validation($data, $files) {
		global $DB;
        
		$errors = parent::validation($data, $files);
		
		//Validate template name.
        if($data['tname'] == "") {
            $errors['tname'] = "Please Insert Template Name.";
            return $errors;
        } else if ( !$data['id'] && $DB->record_exists('block_cedd_sms_template',array('tname' => $data['tname']))) {
            $errors['tname'] = 'Template Name already exists';
            return $errors;
        }
		
		//Validate Course selection.
		if ( !isset($data['courseid'])) {
			 $errors['courseid'] = 'Please select a Session.';
			 return $errors;
		} else if ( isset( $data['ttype'] )) {
			if( is_array( $data['courseid'] )){
				//The combination of Course and Type must be unique.
				//Eg. Session 1 cannot have 2 Morning Type messages.
				foreach ( $data['courseid'] as $courseid ){
					$sql = "SELECT cs.id, cs.courseid, ct.ttype FROM {block_cedd_sms} cs, {block_cedd_sms_template} ct ";
					$sql .= "WHERE cs.templateid = ct.id ";
					$sql .= "AND cs.courseid = ? ";
					$sql .= "AND ct.ttype = ?";
					$params = array( 'courseid' => $courseid, 'ttype' => $data['ttype'] );

					if( !$data['id'] && $DB->record_exists_sql($sql, $params)){
						$errors['courseid'] = 'This Session already has a Message of that type.';
						return $errors;
					
					}
				}
			}
		}
		//All good.
        return true;
    }
	
	
    public function display_report() {
        global $DB, $OUTPUT, $CFG, $USER;
        $table = new html_table();
        $table->head  = array(get_string('serial_no', 'block_cedd_sms'), get_string('name', 'block_cedd_sms'), get_string('msg_body', 'block_cedd_sms'), get_string('edit', 'block_cedd_sms'), get_string('delete', 'block_cedd_sms'));
        $table->size  = array('10%', '20%', '50%', '10%', '10%');
        $table->align  = array('center', 'left', 'left', 'center', 'center');
        $table->width = '100%';
        $table->data  = array();
        $sql="SELECT * FROM {block_cedd_sms_template}";
        $rs = $DB->get_recordset_sql($sql, array());
        
        $i=0;
        foreach ($rs as $log) {
            $row = array();
            $row[] = ++$i;
            $row[] = $log->tname;
            $row[] = $log->template;
            $row[] = '<a  title="Edit" href="'.$CFG->wwwroot.'/blocks/cedd_sms/view.php?viewpage=1&edit=edit&id='.$log->id.'"/><img src="'.$OUTPUT->pix_url('t/edit') . '" class="iconsmall" /></a> ';
            $row[] = '<a  title="Remove" href="'.$CFG->wwwroot.'/blocks/cedd_sms/view.php?viewpage=1&rem=remove&id='.$log->id.'"/><img src="'.$OUTPUT->pix_url('t/delete') . '" class="iconsmall"/></a>';
            $table->data[] = $row;
        }
        return $table;
    }
}