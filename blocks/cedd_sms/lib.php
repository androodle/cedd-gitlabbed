<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/* CEDD SMS Notifications Block
 * CEDD SMS Notifications is a one way SMS messaging block customised for the CEDD project.
 * It allows administrators to send daily SMS reminders to students based on course, at scheduled times
 * in the morning and evening. Based on the SMS Notifier Block by Azmat Ullah, Talha Noor.
 * @package blocks
 * @author: Johanna Follett for AccessMQ <johanna.follett@mq.edu.au>
 * @date: 28-Nov-2014
*/

defined('MOODLE_INTERNAL') || die();


/**
 * This function will send the SMS using Clickatells HTTP API, 
 * with this API Users can send international messages.
 * This was in the original plugin, SMS Notifier Block, but is
 * no longer required, as it has been replaced by
 * {@link cedd_send_sms_clickatell_smtp()} because we need to 
 * to send a large number of SMS at the same time.
 *
 * @param array   $to  mobile numbers to send to
 * @param string $msg  Message Text
 * @return Call back URL through clickatell
 */
function send_sms_clickatell_http($to, $message) {
    global $CFG;
    /*User Numbers*/
    $numbers = '';
    foreach($to as $num){
        if($numbers == '') {
            $numbers =  $num;
        }
        else {
            $numbers .=  ','.$num;
        }
    }

    // Usernames.
    $username = $CFG->block_cedd_sms_api_username;
    // Password
    $password = $CFG->block_cedd_sms_api_password;
    // SMS API.
    $api_id = $CFG->block_cedd_sms_apikey;
    // Send Sms.
    $url = "http://api.clickatell.com/http/sendmsg?user=".$username."&password=".$password."&api_id=".$api_id."&to=".$numbers."&text=".$message;
    redirect($url);
	echo "normally redirect to: " . $url . "\n";
}

/**
 * CEDD_SMS_NUM_DAYS - number of days between Sessions.
 */
define('CEDD_SMS_NUM_DAYS', 5);

/**
 * CEDD_SESSION_CATEGORY_NAME - name of the Course Category that contains Sessions.
 */
define('CEDD_SESSION_CATEGORY_NAME', 'cedd');

/**
 * CEDD_HOURS_BETWEEN_SMS - number of hours between the Morning SMS and Evening SMS
 */
define('CEDD_HOURS_BETWEEN_SMS', 9);

/**
 * Get the available message types.
 * Message types are set here, rather than in a variable.
 *
 * @param bool $select indicate whether to return an array suitable for a form select element
 * @return array $message_types
 */
function cedd_get_message_types( $select = true ){
	$message = "function cedd_get_message_types\n";
	cedd_debug_log( $message );
	
	if( $select ){
		$message_types = array(
					1 => "Morning",
					2 => "Evening",
					3 => "Other"
				);
	} else {
		$message_types = array(
					'morning' => 1,
					'evening' => 2,
					'other' => 3
				);
	}
	return $message_types;
}

/**
 * Get all Session Courses
 * Currently there are 5 Sessions, but this may be extended in future.
 * Use the category so we don't include the eScreen Course (and any other non-Session Courses) 
 *
 * @return array $courses    array of Course Objects
 */
 function cedd_get_session_courses(){
	$message = "function cedd_get_session_courses\n";
	cedd_debug_log( $message );
	
	$cedd_category_id = cedd_get_session_category_id();
    $courses = get_courses( $cedd_category_id, 'idnumber');
	
	return $courses;
}

/**
 * Get the Category ID that contains only Sessions.
 * Sessions are in the category named CEDD_SESSION_CATEGORY_NAME
 * 
 * @return int $cedd_category->id     Session Category ID
 */
function cedd_get_session_category_id(){
	$message = "function cedd_get_session_category_id\n";
	cedd_debug_log( $message );
	
	global $DB;
    $sql_category = "SELECT id from {course_categories} where name='" . CEDD_SESSION_CATEGORY_NAME . "'";
    $cedd_category = $DB->get_record_sql($sql_category);
	return $cedd_category->id;
}

/**
 * Get all Session Course IDs
 *
 * @return array $course_ids  array of Course IDs
 */
function cedd_get_session_course_ids(){
	$message = "function cedd_get_session_course_ids\n";
	cedd_debug_log( $message );
	
	//Get the Course objects
	$courses = cedd_get_session_courses();
	
	$course_ids = array();
	
	foreach ( $courses as $course ){
		$course_ids[] = $course->id;
	}
	
	return $course_ids;
}

/**
 * Get all Users who have completed ANY Sessions
 * Looks up the 'course_completions' table for any
 * Session Course IDs that are found in the given array.
 * 
 * @return array of database results
 */
function cedd_get_users_who_completed_sessions(){
	$message = "function cedd_get_users_who_completed_sessions\n";
	cedd_debug_log( $message );
	
	global $DB;
	
	$session_course_ids = cedd_get_session_course_ids();
	
	$results = $DB->get_records_list( 'course_completions', 'course', $session_course_ids, null, 'id, userid, course');

	return $results;
}

/**
 * Get a list of Users (students) who are enrolled in each Session.
 * 
 * @return 2D array with Course ID as index, and array of enrolled User objects in each Course
 */
function cedd_get_users_enrolled_in_sessions( ){
	$message = "function cedd_get_users_enrolled_in_sessions\n";
	cedd_debug_log( $message );

	//Get the Course objects
	$courses = cedd_get_session_courses();
	//Get the role id for students
	$role_id = cedd_get_role_id( 'student');
	
	$students_list = array();
	//$students_list_B = array(); //Method B
	
	foreach ( $courses as $course ){
		 $context = context_course::instance($course->id);
         $students_list[ $course->id ] = get_role_users( $role_id, $context);
		 
		 //Method B returns same result
		 //$students_list_B[ $course->id ] = get_enrolled_users($context, '', 0, 'u.*', 'u.firstname ASC, u.lastname ASC, u.username ASC');
	}

	return $students_list;
	
}

/**
 * Get list of User IDs (students) who are enrolled in each Session.
 * Uses {@link cedd_get_users_enrolled_in_sessions()} to get the User object
 * and then loops through them to retrieve just the User IDs.
 *
 * @return 2D array with Course ID as index, and array of User IDs in each Course
 */
function cedd_get_user_ids_enrolled_in_sessions(){
	$message = "function cedd_get_user_ids_enrolled_in_sessions\n";
	cedd_debug_log( $message );
	
	$students_list = cedd_get_users_enrolled_in_sessions( );
	
	$users_per_course = array();
	
	foreach( $students_list as $c_id => $student_array ){

		$student_ids = array();
		
		foreach( $student_array as $key => $student ){
			//echo "student = ";
			//print_object($student);
			//echo "key = " . $key . " ";
			$student_ids[] = $key;
		}
		$users_per_course[ $c_id ] = $student_ids;
	}
	//echo "users_per_course = ";
	//print_object( $users_per_course );
	return $users_per_course;
}

/**
 * Get all Users who have completed any Course in the last {$num_days} days
 * Defaults to last 5 days, which is a constant, CEDD_SMS_NUM_DAYS
 *
 * @param int $num_days  How many days ago should be checked. Defaults to 5.
 * @return array of Completion records from database
 */
function cedd_get_users_who_recently_completed_any_course( $num_days = CEDD_SMS_NUM_DAYS ){
	$message = "function cedd_get_users_who_recently_completed_any_course\n";
	cedd_debug_log( $message );
	
	global $DB;
	
	//N.B instead of making five days ago from the current time, which will be 9am when cron is triggered
	//it should actually be from midnight, or 11:59 pm the day before
	//that way we don't include Sessions that were completed between midnight and 9am that day
	
	//Five days ago from midnight today
	$five_days_ago = strtotime( "-" . $num_days ." days" , strtotime("00:00:00"));	
	
	// Now check completion date/time
	$sql = "SELECT id, userid, course, timecompleted FROM {course_completions} WHERE timecompleted > " . $five_days_ago;
	$completion_records = $DB->get_records_sql($sql);
	
	return $completion_records;
}

/**
 * This is added for testing purposes only!
 * Forcefully complete a course for a given User ID.
 * N.B this does not check if the user is enrolled!
 *
 * @param int $user_id User who completed it.
 * @param int $course_id The course that was completed.
 */
function cedd_manually_complete_course( $user_id, $course_id){
	// Load course completion.
	$params = array(
		'userid' => $user_id,
		'course' => $course_id,
	);
	$ccompletion = new completion_completion($params);
	$ccompletion->mark_complete();
}

/**
 * Filter the list of users who have recently completed any course in last 5 days
 * based on whether Course was a Session Course.
 *
 * @param array $completion_records array of Completion records from database
 * @param array $session_course_ids array of Session Course IDs
 * @return array $completion_records with any unwanted elements removed
 */
function cedd_get_users_recently_completed_session( $completion_records, $session_course_ids ){
	$message = "function cedd_get_users_recently_completed_session\n";
	cedd_debug_log( $message );
	
	foreach ( $completion_records as $key => $record ){	
		$context = context_course::instance($record->course);
		
		//If the Course ID for recently completed Course is NOT a Session Course ID
		//OR User is NOT enrolled
		if ( !in_array( $record->course,  $session_course_ids ) || !is_enrolled( $context, $record->userid, '', true)){
			//Remove it from $completion_records
			//so it only contains Session Course IDs
			unset( $completion_records[ $key ]);
		} //else keep it in $completion_records
	}
	
	return $completion_records;
}

/**
 * Each Session Course will have an SMS Message content, associated with it.
 * This is editable in the CMS, based on the SMS Notifier plugin.
 * Each Message can be assigned to multiple Courses.
 * Each Course can have multiple messages, but not of the same type.
 * The Message Type per Course must be unique.
 * Message Names are created in the CMS.
 * 
 * Message Type is 1, 2, or 3.
 * 1 = Morning
 * 2 = Evening
 * 3 = Other
 * Use {@link cedd_get_message_types()} to retrieve the available Message Types.
 *
 * @param int $completed_course_id Course ID that was completed
 * @param int $last_session_id Course ID for the last session. No messages are sent if
 *                             it is the last session.
 * @param int $message_type (see above)
 * @return string|bool message template content OR false if no message found or it is the last Session
 */
function cedd_get_session_sms_message( $completed_course_id, $last_session_id, $message_type ){
	$message = "function cedd_get_session_sms_message\n";
	cedd_debug_log( $message );
	
	global $DB;
	
	//If this is the Last Session
	if( $completed_course_id == $last_session_id ){
		//No message will be sent so abort
		return false;
	}
	
	//Look up in the db table 'block_cedd_sms'
	//Get the template message 
	//Need to join with 'block_cedd_sms_template' table	
	$sql = "SELECT cs.id, cs.templateid, cs.courseid, ct.ttype, ct.template FROM {block_cedd_sms} cs, {block_cedd_sms_template} ct ";
	$sql .= "WHERE cs.templateid = ct.id ";
	$sql .= "AND cs.courseid = " . $completed_course_id;
	$sql .= " AND ct.ttype = " . $message_type;
	$message_records = $DB->get_records_sql($sql, null, 0, 1 );
	//Should return only 1 record, so $limitfrom = 0, $limitnum = 1
	
	//Get the first element in array without key
	$message_row = reset( $message_records ); //returns false if array is empty
	
	if( $message_row ){
		return $message_row->template; //message content
	}
	
	return false; //No message will be sent
}

/**
 * Get Course ID of the last Session.
 * Counts the number of Sessions and compares
 * it with the ID number of each Session.
 *
 * @param array $session_courses array of Session Course objects
 * @return int|bool Course ID of last Session OR false if none found
 */
function cedd_get_last_session_id( $session_courses ){
	$message = "function cedd_get_last_session_id\n";
	cedd_debug_log( $message );
	
	if( is_array( $session_courses ) ){
		$count_sessions = count( $session_courses );
		
		foreach( $session_courses as $session_course ){
			//Get the ID number for the Course
			$session_idnum = $session_course->idnumber;
			//if the id number is same as the count
			if( $count_sessions == $session_idnum ){
				//it's the last session
				return $session_course->id;
			}
		}
	}
	
	return false;
}

/**
 * Get the ID for a given Role name (shortname).
 *
 * @param string $role_name shortname for a Role eg. 'student'
 * @return int Role ID
 */
function cedd_get_role_id( $role_name ){
	$message = "function cedd_get_role_id\n";
	cedd_debug_log( $message );
	
	global $DB;
	//Is there a better way to do this?
	$role_record =  $DB->get_record_sql('SELECT DISTINCT id FROM {role} WHERE shortname = ?', array( $role_name ), $limitfrom=0, $limitnum=1);
	return $role_record->id;
}

/**
 * Get the SMS message template body for a given template ID.
 *
 * @param int $template_id ID of the SMS Message Template
 * @return string template body content as set in CMS.
 */
function cedd_get_template_body( $template_id ){
	$message = "function cedd_get_template_body\n";
	cedd_debug_log( $message );
	
	global $DB;
	//Is there a better way to do this?
	$template_record =  $DB->get_record_sql('SELECT DISTINCT template FROM {block_cedd_sms_template} WHERE id = ?', array( $template_id ), $limitfrom=0, $limitnum=1);
	
	return $template_record->template;
}

/**
 * This is the main function that will be triggered by the Cron Task.
 * 
 * This ties all the other functions together.
 * First, it gets the Session Courses.
 * Next, it gets the Users who have recently completed any Sessions.
 * Next, it loops through the Sessions and sets the message content for each Session.
 * Next, it loops through the Users who have recently completed any Sessions
 * and checks to see if they have completed this particular Session.
 * Then, if it is an Evening message type, go through and check if the User
 * has completed any tools since 9am this morning (when the first reminder was sent).
 * If no, or it is a Morning mandatory message type, then get the Users mobile numbers.
 * If we have mobile numbers and message content, then send the SMS using
 * Clickatell's SMTP API.
 *
 * Message Type is 1, 2, or 3.
 * 1 = Morning
 * 2 = Evening
 * 3 = Other
 * Use {@link cedd_get_message_types()} to retrieve the available Message Types.
 *
 * @param int $message_type The type of SMS Message (see above)
 *
 */
function cedd_sms_cron( $message_type = 1 ){
	$message = "function cedd_sms_cron\n";
	
	global $DB;
	
	//First set up all the variables we need
	//using other functions
	$cedd_session_courses = cedd_get_session_courses();
	$cedd_last_id = cedd_get_last_session_id( $cedd_session_courses );
	$cedd_session_course_ids = cedd_get_session_course_ids(); 
	$cedd_completed_users = cedd_get_users_who_completed_sessions();
	$completion_records = cedd_get_users_who_recently_completed_any_course();
	$recent_completed_session_records = cedd_get_users_recently_completed_session( $completion_records, $cedd_session_course_ids );

	//Loop through Sessions first and set the message content for each Session
	//then Loop through Users in each Session
	foreach ( $cedd_session_course_ids as $cedd_session_course_id ){
		
		$message .= "courseid = " . $cedd_session_course_id . " \n"; //Debugging
		$message_content = cedd_get_session_sms_message( $cedd_session_course_id, $cedd_last_id, $message_type );
		
		if( $message_content ){
			$message .= "Message Content: " . $message_content . "\n";  //Debugging
			$to_mobiles = array(); //This is where we'll store all the mobile numbers
					
			//Loop through each User in recently completed sessions array and get the record object 
			//N.B. that there should only be 1 Course completed in the last 5 days for each User, (if any)
			//since each Session requires that 5 days have lapsed before starting the next Session 
			foreach ( $recent_completed_session_records as $completion_record ){
			
				$userid = $completion_record->userid;
				$completed_course_id = $completion_record->course;

				$message .= "userid = " . $userid . " \n";  //Debugging
				$message .= "completed_course_id = " . $completed_course_id . " \n";  //Debugging
				
				//Check that user has completed this Course
				if( $completed_course_id == $cedd_session_course_id ){
					//Evening messages
					$message_types = cedd_get_message_types( false );
					if( $message_type == $message_types['evening'] ){
						$complete = false;
						//This should be set to 9 am today 
						//Nine hours ago from 6pm is 9am
						$nine_hours_ago = strtotime( "-" . CEDD_HOURS_BETWEEN_SMS ." hours" , time() );
						//Check if the user has completed at least one of any of the Tools
						//since 9am today
						
						//Check Food Diary
						$sql = "Select id from {block_fnt_food} Where userid = ? AND date_created > ?";
						$params = array( 'userid' => $userid, 'date_created' => $nine_hours_ago );
						if ( $DB->record_exists_sql($sql, $params)) {
							//User has completed atleast one entry in food diary
							$complete = true;
						}
						
						//Check Thought Diary
						$sql = "Select id from {block_fnt_thought} Where userid = ? AND date_created > ?";
						$params = array( 'userid' => $userid, 'date_created' => $nine_hours_ago );
						if ( $DB->record_exists_sql($sql, $params)) {
							//User has completed at least one entry in Thought diary
							$complete = true;
						}
						
						//Check Feared Foods Diary
						$sql = "Select id from {block_fnt_fear} Where userid = ? AND date_created > ?";
						$params = array( 'userid' => $userid, 'date_created' => $nine_hours_ago );
						if ( $DB->record_exists_sql($sql, $params)) {
							//User has completed at least one entry in Feared Foods diary
							$complete = true;
						}						
						
					} //end Evening Message checks
					
					//If complete is not set OR set to false OR it is a mandatory morning message
					if(  empty( $complete ) || $message_type == $message_types['morning']){
						//Send a reminder
						$message .= "Send a reminder! ";  //Debugging
						
						//Get users saved mobile number, regular format
						$mobile= cedd_get_user_mobile( $userid );
						//Get users country code
						$country_code = cedd_get_user_country( $userid );
						
						if( $mobile ){
							//Get Users mobile number, in international format eg. 448311234567
							$intl_mobile_number = cedd_get_international_mobile( $mobile, $country_code);
							
							if( $intl_mobile_number ){
								$message .= "Send SMS to: " . $intl_mobile_number;  //Debugging
								$to_mobiles[] = $intl_mobile_number; //Append to array of mobile numbers
							}else {
								$message .= "No international mobile number. Message was not sent. ";  //Debugging
							}
						} else {
							$message .= "User does not have a mobile number saved. Message was not sent. ";  //Debugging
						}
					} else {
						$message .= "No reminder required! ";  //Debugging
					}
					
				} //end if	
			} //end foreach
			
			if( !empty( $to_mobiles ) ){
				//Have mobile numbers
				cedd_send_sms_clickatell_smtp( $to_mobiles, $message_content);
				$message .= "SMS sent!";  //Debugging
			} else {
				$message .= "No SMS sent!";  //Debugging
			}
			
		} //end if

	} //end foreach
	
	cedd_debug_log( $message );  //Debugging
}

/*
 * Get the mobile number database record for a given User
 *
 * @param int $userid id of the User
 * @return object record from database, contains User ID, mobile number, and country code.
 */
function cedd_get_user_mobile_record( $userid ){
	$message = "function cedd_get_user_mobile_record\n";  //Debugging
	cedd_debug_log( $message );  //Debugging
	
	global $DB;
	$sql = "SELECT id, phone2, country FROM {user} WHERE id = ?";
	$mobile_record = $DB->get_record_sql($sql, array ( $userid ));
	return $mobile_record;
}

/**
 * Get the mobile number for a given User
 * This was added to replace function cedd_get_user_mobile_record()
 * that way we can check if the user specifically has a mobile number.
 * 
 * @param int $userid ID of the User
 * @return int mobile phone number that was saved in database
 */
function cedd_get_user_mobile( $userid ){
	$message = "function cedd_get_user_mobile\n";  //Debugging
	cedd_debug_log( $message );  //Debugging
	
	global $DB;
	$sql = "SELECT id, phone2 FROM {user} WHERE id = ?";
	$mobile_record = $DB->get_record_sql($sql, array ( $userid ));
	return $mobile_record->phone2;
}

/**
 * Get the Country Code for a given User.
 * This was also added after replacing function cedd_get_user_mobile_record().
 * Get the user's country code separately.
 * 
 * @param int $userid  ID of the User
 * @return string Moodle Country Code eg. 'AU' for Australia
 */
function cedd_get_user_country( $userid ){
	$message = "function cedd_get_user_country\n";  //Debugging
	cedd_debug_log( $message );  //Debugging
	
	global $DB;
	$sql = "SELECT id, country FROM {user} WHERE id = ?";
	$country_record = $DB->get_record_sql($sql, array ( $userid ));
	return $country_record->country;
}

/**
 * Convert a mobile number into international format, given a country code
 * 
 * @param int $mobile_number Mobile number in the following format: eg. 0418583234
 * @param string  $country_code Two character country code, eg. AU for Australia
 * @return string|bool $intl_number Mobile number in international format eg. 61418583234
 * 									OR false if country code is not AU or no mobile number given
 */
function cedd_get_international_mobile( $mobile_number, $country_code="AU" ){
	$message = "function cedd_get_international_mobile\n";  //Debugging
	cedd_debug_log( $message );  //Debugging
	
	//Validate the mobile number
	//TODO: This also needs to be done when it is first entered at Create Account or eScreen
	 if (strlen( $mobile_number )!=10 || substr($mobile_number, 0, 2) != "04"){
			$message = "ERROR: Phone number is not valid. Must be 10 characters and start with '04'.\n";   //Debugging
			cedd_debug_log( $message );   //Debugging
			return false;
	}

	
	if( $mobile_number ){
		//Then if valid,
		//Convert it to International Format
		if ( $country_code == "AU" ){
			//Remove zero and Add Australian Country Code 61
			//Cast as a string
			$mobile_string = (string)$mobile_number;
			$intl_number = preg_replace('/^0/','61',$mobile_string);
			return $intl_number;
		} else {
			$message = "ERROR: Country code must be for Australia only.\n";  //Debugging
			cedd_debug_log( $message );  //Debugging
		}
	}
	return false;
}

function cedd_save_message( $data ){
	global $DB;
	
	$record_ids = array(); //We'll use this later
	
	if( is_object( $data )){
		
		//////////////// EDITING MESSAGE ////////////////////////////////
		//Check if we are editing
		if( $data->id ){
			//Editing Message
			//First, update the Message template table
			$msg_id = $DB->update_record('block_cedd_sms_template', $data);		
			
			//Update was successful
			//Now also save a record in table 'block_cedd_sms'
			if( $msg_id ){
				$data->templateid = $data->id;
				
				//1. Get the new courses
				if( isset( $data->courseid )){
					if( is_array( $data->courseid )){
						$new_courses = $data->courseid;
					}else {
						$new_courses = array( $data->courseid );
					}
					//N.B that new_courses is an array of integers
				
					//2. Get the old courses
					$sql = "SELECT id, courseid, templateid FROM {block_cedd_sms} WHERE templateid = ?";
					$params = array( 'templateid' => $data->templateid );
					$old_course_records = $DB->get_records_sql($sql, $params);
					//N.B that $old_course_records is an array of objects

					//Convert array of objects into array of integers
					$old_courses = array();

					foreach( $old_course_records as $old_course ){
						$old_courses[] = $old_course->courseid;
					}
					
					//3. Compare the old courses with the new
					$todelete = array_diff($old_courses, $new_courses);
					//echo "todelete = ";
					//print_object( $todelete );
					
					if( !empty( $todelete ) ){
						//loop through and delete records
						foreach( $todelete as $delete ){
							$DB->delete_records('block_cedd_sms', array('courseid'=>$delete, 'templateid' =>$data->templateid));
						}
					}
					
					//4. Now do the reverse, compare the new with the old
					$toadd = array_diff($new_courses, $old_courses);
					//echo "to add = ";
					//print_object( $toadd );
					
					if( !empty( $toadd ) ){
						//loop through and Add records
						foreach( $toadd as $add ){
							$record_ids[] = $DB->insert_record('block_cedd_sms', array('courseid'=>$add, 'templateid' =>$data->templateid));
						}
					}
					
				} //end if
			}
			
		////////////////////ADDING NEW MESSAGE/////////////////////////////////	
		}else {
		
			//Adding New Message
			//First, insert a new record into the Message template table
			$msg_id = $DB->insert_record('block_cedd_sms_template', $data);
			
			//Insert was successful
			//Now also save a record in table 'block_cedd_sms'
			if( $msg_id ){
				$data->templateid = $msg_id;
				
				//Now process courses
				if( isset( $data->courseid )){
					$courseids = $data->courseid;
					
					if( is_array( $courseids )){
						foreach( $courseids as $courseid ){
							
							$data->courseid = $courseid; //Just want an integer to save in database
							$record_ids[] = $DB->insert_record('block_cedd_sms', $data);
						} //end foreach
					}else {
						//Just one course
						$data->courseid = $courseids;
						$record_ids[] = $DB->insert_record('block_cedd_sms', $data);
					} //end if else
				} //end if
			} //end if
		} //end if else
		
	} //end if
	return $record_ids;
}

function cedd_send_sms_clickatell_smtp($to, $text_message) {
	$message = "function cedd_send_sms_clickatell_smtp\n";

	
    global $CFG;
    /*User Numbers*/
    //http://www.sourcexref.com/xref/moodle/nav.html?lib/moodlelib.php.source.html#l5288
    $numbers = '';
	$count = 0;
	
	if( is_array( $to )){
		foreach($to as $num){
			if( $count == 0 ){
				$numbers =  $num;
			}else {
				$numbers .=  ','.$num;	
			}
			$count++;
		}
	}
	$message .= "numbers = " . $numbers;

    // Usernames.
    $username = $CFG->block_cedd_sms_api_username;
    // Password
    $password = $CFG->block_cedd_sms_api_password;
    // SMS API.
    $api_id = $CFG->block_cedd_sms_apikey;
	
		$email_body = "";
		$email_body .= "api_id:" . $api_id . "\n";
		$email_body .= "user:" . $username . "\n";
		$email_body .= "password:" . $password . "\n";
		$email_body .= "to:" . $numbers . "\n";
		$email_body .= "text:" . $text_message . "\n";
	
	$message .= "email_body = " . $email_body;
	
	$success = cedd_send_email_to_clickatell( $email_body );
	if( $success ){
		$message .= "Email sent!";
		echo "Email sent!\n";
		echo "to:" . $numbers . "\n";
		echo "text:" . $text_message . "\n\n";
	} else {
		$message .= "Email not sent!";
	}
    // Send Sms.
    $url = "http://api.clickatell.com/http/sendmsg?user=".$username."&password=".$password."&api_id=".$api_id."&to=".$numbers."&text=".$text_message;
    //redirect($url);
	$message .= "normally redirect to: " . $url . "\n";
	cedd_debug_log( $message );
}

function cedd_send_email_to_clickatell( $email_body ){
	$message = "function cedd_send_email_to_clickatell\n";
	cedd_debug_log( $message );
	
	global $CFG;
    
    // Fetch the PHP mailing functionality
    //include_once($CFG->libdir .'/phpmailer/class.phpmailer.php');
    
	$mail = get_mailer();
    
	$mail->SMTPDebug = 3; //for debuggin
    
    if (!empty($mail->SMTPDebug)) {
        $message = '<pre>' . "\n";
    }   
    
    $temprecipients = array();
    $tempreplyto = array();
	
	$supportuser = core_user::get_support_user();
	//$mail->Sender = $supportuser->email; //not required
	$mail->From     = $supportuser->email;
    $mail->FromName = 'CEDD';
    
    $tempreplyto[] = array($CFG->noreplyaddress, get_string('noreplyname'));
    //$mail->Subject = "Test Subject";
    
    
	
	//Ignored by clickatell
	//$mail->Subject = substr($subject, 0, 900); //not required
	// Set word wrap.
    //$mail->WordWrap = 40;
	//$message .= "wordwrap: " . $mail->WordWrap;
	
	$mail->IsHTML(false);    
    $mail->Body =  "\n$email_body\n";
	//$mail->AddAddress('sms@messaging.clickatell.com',"");	//comment out so sms can be tested sending via email.  SMS message structure has been tested successfully with Clickatel
    $mail->AddAddress('netduet.testing@gmail.com',"");
          
    
	$message .= "Start sending:\n";
	cedd_debug_log( $message );
	 if ($mail->Send()) {
		
        if (!empty($mail->SMTPDebug)) {
            $message = '</pre>';
			cedd_debug_log( $message );
        }
		$message = "Return:\n";
		cedd_debug_log( $message );
        return true;
    } else {
        if (!empty($mail->SMTPDebug)) {
            $message = '</pre>';
			cedd_debug_log( $message );
        }
        return false;
    }
}

function cedd_debug_log( $message ) {
	//$debug_file = "cedd_debug_log.txt";
	global $CFG;
	
    if ( $CFG->debugdeveloper ) {
        if ( is_array( $message ) || is_object( $message )) {
			$object = print_r($message, true);
			//file_put_contents($debug_file, $object, FILE_APPEND ); //Debugging
			error_log( $object );
        } else {
			//file_put_contents($debug_file, $message , FILE_APPEND ); //Debugging
			error_log( $message );
        }
    }	
}
