<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/* CEDD SMS Notifications Block
 * CEDD SMS Notifications is a one way SMS messaging block customised for the CEDD project.
 * It allows administrators to send daily SMS reminders to students based on course, at scheduled times
 * in the morning and evening. Based on the SMS Notifier Block by Azmat Ullah, Talha Noor.
 * @package blocks
 * @author: Johanna Follett for AccessMQ <johanna.follett@mq.edu.au>
 * @date: 28-Nov-2014
*/

class block_cedd_sms_edit_form extends block_edit_form {

    protected function specific_definition($mform) {
        // Section header title according to language file.
        $mform->addElement('header', 'configheader', get_string('blocksettings', 'block'));
        // A sample string variable with a default value.
        $mform->addElement('textarea', 'config_text', 'Embeded Code');
        $mform->setDefault('config_text', 'default value');
        $mform->setType('config_text', PARAM_MULTILANG);
        $mform->addElement('text', 'config_title', get_string('blocktitle', 'block_cedd_sms'));
        $mform->setDefault('config_title', 'default value');
        $mform->setType('config_title', PARAM_MULTILANG);

    }
}