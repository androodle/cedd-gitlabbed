<?php 

namespace block_cedd_sms\task;

class cedd_send_morning_sms extends \core\task\scheduled_task {      
    public function get_name() {
        // Shown in admin screens
        return get_string('cedd_send_morning_sms', 'block_cedd_sms');
    }
                                                                     
    public function execute() {       
        global $CFG;
        require_once($CFG->dirroot . '/blocks/cedd_sms/lib.php');
		$message_types = cedd_get_message_types( false );
		$message_type = $message_types['morning']; //Test Morning Message
		cedd_sms_cron( $message_type );
    }                                                                                                                               
} 