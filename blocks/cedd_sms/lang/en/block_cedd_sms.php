<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/* SMS Notifier Block
 * SMS notification is a one way SMS messaging block that allows managers, teachers and administrators to
 * send text messages to their student and teacher.
 * @package blocks
 * @author: Azmat Ullah, Talha Noor
 * @date: 06-Jun-2013
*/

    $string['pluginname'] = 'CEDD SMS Notifier';
	$string['blocktitle'] = 'CEDD SMS Notifier';
    $string['cedd_sms'] = 'CEDD SMS Notifier';
    $string['sms_send'] = 'Send SMS';
	$string['sms_view'] = 'View SMS Messages';
	$string['sms_create'] = 'Create New SMS Message';
    $string['addpage'] = 'Add Page';
    $string['textfields'] = 'Data';
    $string['edithtml'] = 'Data Manipulation';
    $string['pagetitle'] = 'Input Text';
    $string['picturedesc'] = 'Description';
    $string['sms_save'] = 'Message Template';
    $string['sms_history'] = 'Message History';
    $string['selectcourse'] = 'Select Session';
    $string['selectrole'] = 'Select Role';
    $string['sms_body'] = 'Message Body';
    $string['selectmsg'] = 'Select Message';
    $string['template'] = 'Create Message Template';
    
    $string['sms_template_header'] = 'New Template';
    $string['sms_api_key'] = 'API Key';
    $string['sms_api_username'] = 'Username';
    $string['sms_api_password'] = 'Password';
    $string['sms_setting_header'] = 'CEDD SMS Notification Settings';
    $string['block_cedd_sms_apikey'] = 'block_cedd_sms_apikey';
    $string['block_cedd_sms_api_username'] = 'block_cedd_sms_api_username';
    $string['block_cedd_sms_api_password'] = 'block_cedd_sms_api_password';
    $string['error'] = 'Error';
    $string['sent'] = 'Sent';
    $string['valid_key'] = 'Your key IS VALID';
    $string['serial_no'] = 'Serial No.';
    $string['name'] = 'Name';
    $string['msg_body'] = 'Message Body';
    $string['edit'] = 'Edit';
    $string['delete'] = 'Delete';
    $string['cell_no'] = 'Cell Number';
    $string['select'] = 'Select';
    $string['askfordelete'] = 'Do You Want to Delete This Template?';
    $string['moodleuser'] = 'Username';
    $string['usernumber'] = 'Numbers';
    $string['status'] = 'Status';
	$string['cedd_send_morning_sms'] = 'CEDD Send Morning SMS';
	$string['cedd_send_evening_sms'] = 'CEDD Send Evening SMS';
	$string['cedd_set_session_message'] = 'CEDD Set SMS for each Session';
	$string['cedd_assign_sms'] = 'CEDD Assign Message to a Session';
	$string['cedd_datasubmitted'] = 'The data has been submitted';
	$string['cedd_proceed'] = 'Proceed';
	$string['selecttype'] = 'Select Type';
	// Permissions
	$string['cedd_sms:addinstance']= 'Add Instance';
	$string['cedd_sms:viewpages']= 'View Pages';
	$string['cedd_sms:myaddinstance']= 'My Add Instance';
	$string['cedd_sms:managepages']= 'Manage Pages';
	
	
    
    




        