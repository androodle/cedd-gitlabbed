<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/* CEDD SMS Notifications Block
 * CEDD SMS Notifications is a one way SMS messaging block customised for the CEDD project.
 * It allows administrators to send daily SMS reminders to students based on course, at scheduled times
 * in the morning and evening. Based on the SMS Notifier Block by Azmat Ullah, Talha Noor.
 * @package blocks
 * @author: Johanna Follett for AccessMQ <johanna.follett@mq.edu.au>
 * @date: 28-Nov-2014
*/

require_once('../../config.php');
require_once('sms_form.php');
require_once("lib.php");
// Global variable.
global $DB, $OUTPUT, $PAGE, $CFG, $USER;
require_login();

// Plugin variable.
$viewpage = optional_param('viewpage', null, PARAM_INT);
$rem = optional_param('rem', null, PARAM_RAW);
$edit = optional_param('edit', null, PARAM_RAW);
$delete = optional_param('delete', null, PARAM_RAW);
$id = optional_param('id', null, PARAM_INT);

$urlparams = array('viewpage' => $viewpage);

// Page settings.
$context = context_system::instance(); //J.F added.
$PAGE->set_context($context); //J.F added.


$PAGE->set_pagelayout('standard');
$PAGE->set_title(get_string("pluginname", 'block_cedd_sms'));
$PAGE->set_heading('CEDD SMS Notification');

$pageurl = new moodle_url('/blocks/cedd_sms/view.php');
$PAGE->set_url('/blocks/cedd_sms/view.php', $urlparams);


$form = new template_form();

if( $form->is_cancelled() ){
	redirect($pageurl);
} else if($data = $form->get_data()) { 
	$record_ids = cedd_save_message( $data );
	redirect($pageurl);
}else {
	echo $OUTPUT->header();
	
	//Display the Create New Message Page.
	if ($viewpage == 1) {
		
		///Remove the Message.
		if($rem) {
			//Really delete it?
			if($delete) {
				global  $DB;
				$DB->delete_records('block_cedd_sms_template', array('id'=>$delete));
				$DB->delete_records('block_cedd_sms', array('templateid'=>$delete));
				redirect($pageurl);
			}
			else {
				  echo $OUTPUT->confirm(get_string('askfordelete', 'block_cedd_sms'), '/blocks/cedd_sms/view.php?viewpage=1&rem=rem&delete='.$id, '/blocks/cedd_sms/view.php?viewpage=1');
			}
		}
		// Edit Message Template.
		if($edit) {
			$record_data = $DB->get_record('block_cedd_sms_template', array('id'=>$id), '*');
			$sql = "SELECT * FROM {block_cedd_sms} WHERE templateid = ?";
			$session_results = $DB->get_records_sql($sql, array('templateid'=>$id));
			
			if( !empty( $session_results ) && is_array( $session_results ) ){
					$record_data->courseid = array();
				foreach( $session_results as $record ){
					$record_data->courseid[] = $record->courseid;
				}
			}
			$form->set_data($record_data);        
		}
		
		$toform['viewpage'] = $viewpage;
		$form->set_data($toform);
		//Display the form.
		$form->display();
		
	} //end Create New Message Page.

	//Display the table.
	$table=$form->display_report();
	echo html_writer::table($table);

	echo $OUTPUT->footer();
}